@extends('layouts.frontend.app')

@section('content')
    <link rel="stylesheet" href="{{asset('frontend/css/payment.css')}}">
    <div class="container-wrapper">
        <div class="page-title-wrapper">
            <div class="center">
                <div class="page-title">
                    <div class="page-title-preimage"><img alt="Pretitle-image" src="{{asset('frontend/images/pizza.svg') }}"></div>
                    <h1>Orden</h1>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
        <div class="clear"></div>
        <div id="container">
            <!-- start container -->
            <div class="center">
                <div class="page-wrapper">
                    <div class="main">

                        <main class="page payment-page">
                            <section class="payment-form dark">
                                <div class="container">
                                    <div class="block-heading">
                                        <h2 class="text-danger">Realizar pago</h2>
                                        <!--p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc quam urna, dignissim nec auctor in, mattis vitae leo.</p-->
                                    </div>
                                    <form>
                                        <div class="products">
                                            <h3 class="title">Orden</h3>

                                            @foreach(\Cart::content() as $item)
                                                <div class="item">
                                                    <span class="price">{{$item->price}}Bs.S</span>
                                                    <p class="item-name">{{$item->name}}</p>
                                                    <p class="item-description">
                                                        @if(isset($item->options->toppingId))
                                                           @for($i=0;$i<=count($item->options->toppingId) -1;$i++)
                                                                {!! $toppings[$item->options->toppingId[$i]]->name !!} (x {!! $item->options->toppingQty[$i] !!})
                                                            @endfor
                                                         @endif
                                                    </p>
                                                </div>
                                            @endforeach
                                            <div class="total">Iva (16%)<span class="price">{{Cart::tax()}}   Bs.S</span></div>

                                            <div class="total">Subtotal<span class="price">{{Cart::subtotal()}}   Bs.S</span></div>
                                            <div class="total bg-danger pt-3 pb-3 pl-1 pr-1 text-white" style="font-weight: normal;">Total<span class="price" style="font-weight: normal;">{{Cart::total()}}   Bs.S</span></div>

                                        </div>

                                        @auth
                                        <div class="col-sm-12 products">
                                            <h4 class="title">Datos Personales</h4>
                                            <div class="row">
                                                <div class="form-group col-sm-6">
                                                    <label for="name">Nombres</label>
                                                    <input id="name" type="text" class="form-control" placeholder="Nombres" aria-label="Nombres" aria-describedby="basic-addon1" value="{{Auth::user()->person["name"]}}" readonly >
                                                </div>
                                                <div class="form-group col-sm-6">
                                                    <label for="lastname">Apellidos</label>
                                                    <input id="lastname" type="text" class="form-control" placeholder="Apellidos" aria-label="Apellidos" aria-describedby="basic-addon1" value="{{Auth::user()->person["lastname"]}}"readonly>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group col-sm-6">
                                                    <label for="name">Teléfono</label>
                                                    <input id="telefono" type="text" class="form-control" placeholder="Teléfono" aria-label="Teléfono" aria-describedby="basic-addon1"value="{{Auth::user()->person["phone"]}}" readonly>
                                                </div>
                                                <div class="form-group col-sm-6">
                                                    <label for="correo">Correo</label>
                                                    <input id="correo" type="text" class="form-control" placeholder="Correo" aria-label="Correo" aria-describedby="basic-addon1" value="{{Auth::user()->email}}"readonly>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group col-sm-12">
                                                    <label for="direccion">Dirección</label>
                                                    <textarea class="form-control" placeholder="Dirección" readonly id="address">{{Auth::user()->person["address"]}}</textarea>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="form-group col-sm-12">
                                                    {{--<button type="button" class="btn btn-danger bg-danger white-text btn-block">¿Ya tienes cuenta? Ingresa aquí</button>--}}
                                                </div>
                                            </div>
                                        </div>
                                        @endauth
                                        @guest
                                            <div class="col-sm-12 products">
                                                <div class="alert bg-danger text-white white-text">
                                                    <p class="text-white">Para continuar con la compra necesitas <a class="btn btn-danger" data-toggle="modal" data-target="#loginModal">Logearte</a></p>
                                                </div>
                                            </div>

                                        @endguest

                                        @auth

            

                                        <div class="col-sm-12 products">
                                            <h4 class="title">Dirección de Envío</h4>

                                            @include('frontend.partials.addresspicker')

                                        </div>
                                        
                                            <div class="col-sm-12 products">
                                            <h4 class="title">Métodos de Pago</h4>

                                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                                <li class="nav-item">
                                                    <a class="nav-link active" id="transferencia-tab" data-toggle="tab" href="#transferencia" role="tab" aria-controls="transferencia" aria-selected="true">Tarjeta</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" id="cash-tab" data-toggle="tab" href="#cash" role="tab" aria-controls="cash" aria-selected="false">Efectivo</a>
                                                </li>

                                            </ul>

                                            <div class="tab-content" id="myTabContent">
                                                <div class="tab-pane fade show active" id="transferencia" role="tabpanel" aria-labelledby="transferencia-tab">
                                                    <div class="card-details">


                                                        <h6 class="title">Transferencia Bancaria</h6>
                                                        <div class="row">

                                                            <div class="alert bg-primary">
                                                                <p class="text-white"><i class="fa fa-info-circle"></i> Para proceder con el pago, realiza una transferencia al siguiente número de cuenta y adjunta el número de referencia para procesar con el envío</p>

                                                            </div>
                                                        </div>
                                                        <div class="row">

                                                                <div class="alert bg-success col-sm-12">
                                                                    <p class="text-white"><strong>Banco: </strong>Bicentenario</p>
                                                                    <p class="text-white"><strong>Cuenta Nº: </strong>01750073270075859065</p>
                                                                    <p class="text-white"><strong>Cuenta: </strong>Corriente</p>
                                                                    <p class="text-white"><strong>Páguese a Nombre de: </strong>Randy Gil - V26428919</p>
                                                                </div>


                                                        </div>
                                                        <div class="row">

                                                           <div class="form-group col-sm-12">
                                                                <label for="referencia">Número de Referencia</label>
                                                                <input id="referencia" type="number" class="form-control" placeholder="Referencia" aria-label="Referencia" aria-describe
                                                                min="0" 
                                                                dby="basic-addon1">
                                                            </div>
                                                        </div>



                                                        <div class="form-group col-sm-12">
                                                            <button id="submit-transferencia" type="button" class="btn btn-danger bg-danger white-text btn-block">Terminar</button>
                                                        </div>

                                                </div>
                                            </div>







                                                    <div class="tab-pane fade" id="cash" role="tabpanel" aria-labelledby="cash-tab">
                                                    <div class="card-details">
                                                        <h1 class="title">Pago en Efectivo</h1>
                                                        <p>El pago en efectivo se lo facilitará al repartidor en cuestión cuando su orden llegue.</p>
                                                        <div class="form-group col-sm-12">
                                                            <button id="submit-cash" type="button" class="btn btn-danger bg-danger white-text btn-block">Terminar</button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">...</div>
                                            </div>
                                        </div>
                                        @endauth
                                    </form>
                                </div>
                            </section>
                        </main>

                    </div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
        <!-- end container -->
        <div class="clear"></div>
    </div>
    <!-- end container-wrapper -->


    @push('scripts')
        <script>
            let address_info,axiosData;

            $("#submit-cash").click((e) => {
                e.preventDefault();
                if (validateDelivery()) {

                    sendPost({
                        address_info:$("#address-info").val(),
                        latLong,
                        paymentMethod:1
                    });
                }
            });

            $("#submit-transferencia").click((e) => {
                e.preventDefault();
                let ref = $("#referencia").val();
                if (!ref) {
                    toastr.error('Especifica el número de referencia del pedido');
                    return;
                }
                sendPost({
                    address_info:$("#address-info").val(),
                    latLong,
                    ref,
                    paymentMethod: 2
                });

            });

            function sendPost(post) {
                if (validateDelivery()) {
                    axios.post('{{url('checkout/makeOrder')}}', post).then((data) => {
                        console.log(data.data);
                        data = data.data;
                        if (data.success) {

                            window.location = data.redirect;

                        }
                    }).catch((data) => {
                        console.log(data);
                    });
                }
            }
            function validateDelivery() {

                address_info = $("#address-info").val();
                if (!latLong.lat || !latLong.lng || !address_info) {
                    toastr.error("Especifica la dirección de envío");
                    return false;
                }

                return true;
            }
        </script>
    @endpush
@endsection
