<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 26 Aug 2018 01:29:07 +0000.
 */

namespace App;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Pizza
 * 
 * @property int $id
 * @property string $name
 * @property string $description
 * @property string $image
 * @property int $sale_price
 * @property int $pizzas_categories_id
 * 
 * @property \App\PizzasCategory $pizzas_category
 * @property \Illuminate\Database\Eloquent\Collection $orders
 * @property \Illuminate\Database\Eloquent\Collection $pizza_prices
 *
 * @package App
 */
class Pizza extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'sale_price' => 'int',
		'pizzas_categories_id' => 'int'
	];

	protected $fillable = [
		'name',
		'description',
		'image',
		'sale_price',
		'pizzas_categories_id'
	];

	public function pizzas_category()
	{
		return $this->belongsTo(\App\PizzasCategory::class, 'pizzas_categories_id');
	}

	public function orders()
	{
		return $this->belongsToMany(\App\Order::class, 'orders_pizza', 'pizzas_id', 'orders_id')
					->withPivot('id');
	}

	public function pizza_prices()
	{
		return $this->hasMany(\App\PizzaPrice::class);
	}
}
