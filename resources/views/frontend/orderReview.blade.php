@extends('layouts.frontend.app')

@section('content')

    <link rel="stylesheet" href="{{asset('frontend/css/payment.css')}}">
    <main class="page payment-page mb-5" style="width: 40%; margin: auto;">
        <section class="payment-form dark">
            <div class="container">
                <div class="block-heading">
                    <h2 class="text-danger">Orden # {{$order->id}}</h2>
                    <!--p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc quam urna, dignissim nec auctor in, mattis vitae leo.</p-->
                </div>
                <form>
                    <div class="products">
                        <div class="d-flex row justify-content-center">
                                <a href="{{url('/checkout/orderFactura/'.$order->id)}}" class="btn btn-primary" ><i class="fa fa-download"></i> Descargar Factura Fiscal</a>
                        </div>
                        <h3 class="title">Carta</h3>
                        @foreach($pizzas as $pizza)
                            <div class="item">


                                <span class="price">{{$pizza->price}} Bs.S</span>
                                <p class="item-name">{{$pizza->pizza->name}}</p>
                                <p class="item-description">

                                    @foreach($pizza->orders_pizza_toppings as $topping)
                                        {{$topping->pizzas_topping->name}} (x{{$topping->quantity}})
                                    @endforeach


                                </p>
                            </div>
                        @endforeach

                        <div class="total">Subtotal<span class="price">{{$order->total}}   Bs.S</span></div>
                        <div class="total">Iva ({{$order->iva}}%) <span class="price"> {{$order->total * ($order->iva / 100)}} Bs.S</span></div>


                        <div class="total bg-danger pt-3 pb-3 pl-1 pr-1 text-white" style="font-weight: normal;">Total<span class="price" style="font-weight: normal;">{{$order->total + $order->total * ($order->iva / 100)}}   Bs.S</span></div>
                    </div>



                    <div class="products">
                        <h3 class="title">Dirección de envío</h3>
                        <div id="map"></div>
                        <h6><strong>Información de la dirección:</strong> {{$order->address_info}}</h6>
                    </div>

                    <div class="col-sm-12 products">
                        <h4 class="title">Método de Pago </h4>

                        <h6><i class="fa fa-info-circle"></i> {{$order->payments->payment_method->name}}:</h6>
                        @if($order->payments->payment_methods_id==2)
                            <p><strong>Número de referencia: </strong> {{$order->payments->reference}}</p>
                        @endif

                    </div>
                </form>
            </div>
        </section>
    </main>
    @push('styles')
        <link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.4/dist/leaflet.css"
              integrity="sha512-puBpdR0798OZvTTbP4A8Ix/l+A4dHDD0DGqYW6RQ+9jxkRFclaxxQb/SJAWZfWAkuyeQUytO7+7N4QKrDh+drA=="
              crossorigin=""/>
    @endpush
    <style>

        #map {
            position: relative;
            width: 100%;
            padding-bottom: 56.25%; /* Ratio 16:9 ( 100%/16*9 = 56.25% ) */
        }
        #map  > *{
            display: block;
            position: absolute;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            margin: 0;
            padding: 0;
            height: 100%;
            width: 100%;
        }
    </style>
    @push('scripts')

        <!-- Make sure you put this AFTER Leaflet's CSS -->
        <script src="https://unpkg.com/leaflet@1.3.4/dist/leaflet.js"
                integrity="sha512-nMMmRyTVoLYqjP9hrbed9S+FzjZHW5gY1TWCHA5ckwXZBadntCNs8kEqAWdrb9O7rxbCaA4lKTIWjDXZxflOcA=="
                crossorigin=""></script>
        <script>
            //            var mymap = L.map('mapid').setView([51.505, -0.09], 13);

            var map = L.map('map').setView([{{$order->deliver_lat}}, {{$order->deliver_lng}}], 17);
            L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoicmFuZHlnaWw5NyIsImEiOiJjamxmbGV4Y28weHhjM3dveWJwa256MTM4In0.dfpdyLY4ke8E5JPKuFTauA', {
                attribution: '',
                maxZoom: 30,
                minzoom: 230,
                id: 'mapbox.streets',
                accessToken: 'pk.eyJ1IjoicmFuZHlnaWw5NyIsImEiOiJjamxmbGV4Y28weHhjM3dveWJwa256MTM4In0.dfpdyLY4ke8E5JPKuFTauA'
            }).addTo(map);
            console.log(map);
            L.marker([{{$order->deliver_lat}}, {{$order->deliver_lng}}])
                .addTo(map)
                .bindPopup('Orden #{{$order->id}}')
                .openPopup();
        </script>

    @endpush


@endsection