<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderPizza extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'price' => $this->price,
            'special_preparation' => $this->special_preparation,
            'pizza' => new Pizzas($this->pizza),
            'toppings' => OrdersPizzaTopping::collection($this->whenLoaded('orders_pizza_toppings'))
            //'pizza' => new Pizza($this->pizza)
        ];
    }
}
