<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToOrdersPizzaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('orders_pizza', function(Blueprint $table)
		{
			$table->foreign('orders_id', 'fk_orders_pizza_orders_id')->references('id')->on('orders')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('pizzas_id', 'fk_orders_pizza_pizza_id')->references('id')->on('pizzas')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('orders_pizza', function(Blueprint $table)
		{
			$table->dropForeign('fk_orders_pizza_orders_id');
			$table->dropForeign('fk_orders_pizza_pizza_id');
		});
	}

}
