<?php

use Illuminate\Database\Seeder;
use App\User;
use Spatie\Permission\Models\Role;
use App\Person;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $admin = Role::create(['name' => 'admin']);
        $cliente = Role::create(['name' => 'cliente']);
        $repartidor = Role::create(['name' => 'repartidor']);
        $empleado = Role::create(['name' => 'empleado']);

        $person = Person::create([
            'id' => 1,
           'name' => 'Randy',
           'lastname' => 'Gil',
           'phone' => '04143461587',
           'address' => 'Por ahi',
           'gender' => 'M',
           'born_date' => '1997-05-20',
           'document_type' => 'V',
           'document_number' => '26428919'
        ]);
        $user = User::create(["id" => 1,"username" => "randy","email" => "r.gilcamejo@gmail.com","password" => \Hash::make('123456'),'persons_id' => $person->id]);

        $user->assignRole('admin');

        $person = Person::create([
            'id' => 2,
            'name' => 'Pedro',
            'lastname' => 'Perez',
            'phone' => '04143461587',
            'address' => 'Por ahi',
            'gender' => 'M',
            'born_date' => '1997-05-20',
            'document_type' => 'V',
            'document_number' => '2542345'
        ]);
            $user = User::create(["id" =>2,"username" => "pedro","email" => "pedro@gmail.com","password" => \Hash::make('123456'),'persons_id' => $person->id]);

        $user->assignRole('repartidor');


        $person = Person::create([
            'id' => 3,
            'name' => 'Jefferson',
            'lastname' => 'Gutierritos',
            'phone' => '04125487945',
            'address' => 'Avenida Alonzo número 3878, Encino, California',
            'gender' => 'M',
            'born_date' => '1997-05-20',
            'document_type' => 'V',
            'document_number' => '12412234'
        ]);
        $user = User::create(["id" => 3,"username" => "jefferson","email" => "jefferson@gmail.com","password" => \Hash::make('123456'),'persons_id' => $person->id]);

        $user->assignRole('empleado');

        $person = Person::create([
         'id' => 4,
        'name' => 'Alvin',
        'lastname' => 'Yakitori',
        'phone' => '04168457845',
        'address' => 'Avenida Alonzo número 3878, Encino, California',
        'gender' => 'M',
        'born_date' => '1997-05-20',
        'document_type' => 'V',
        'document_number' => '12412234'
    ]);
        $user = User::create(["id" => 4,"username" => "alvin","email" => "alvin@gmail.com","password" => \Hash::make('123456'),'persons_id' => $person->id]);

        $user->assignRole('repartidor');

    }
}
