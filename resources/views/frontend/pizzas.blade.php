@extends('layouts.frontend.app')
@section('content')
     <div class="container-wrapper">
        <div class="page-title-wrapper">
            <div class="center">
                <div class="page-title">
                    <div class="page-title-preimage"><img alt="Pretitle-image" src="{{asset('frontend/images/pizza.svg')}}"></div>
                    <h1>Pizzas</h1>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
        <div class="clear"></div>
        <div id="container">
            <!-- start container -->
            <div class="center">
                <div class="page-wrapper">
                    <div class="main">
                        <div id="post-8" class="post-8 page type-page status-publish hentry">
                            <div class="vc_row wpb_row vc_row-fluid">
                                <div class="wpb_column vc_column_container vc_col-sm-12">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div class="wpb_content_element vc_portfolio_items">

                                                    @foreach($categories as $category)
                                                    <div class="portfolio-item-separator portfolio-item-separator3">

                                                        <div class="clear"></div>
                                                        <h1> {{$category->name}}</h1>
                                                    </div>

                                                    <div class="portfolio-items-wrapper">
                                                        @foreach($category->pizzas as $pizza)
                                                        <div class="portfolio-items-single columns3"><img class="portfolio-frame" src="{{asset('frontend/images/frame.png')}}" />
                                                            <div class="portfolio-frame-shadow"></div>
                                                            <a class="portfolio-url-thumb" href="#" title="Margarita">
                                                                <img class="portfolio-inner-frame" src="{{asset('frontend/images/inner-shadow.png')}}" />
                                                                <div class="view-overlay-icon pe-7s-plus"></div>
                                                                <div class="view-overlay-bg"></div>
                                                            </a>
                                                            <div class="portfolio-items-single-thumb" href="#" title="{{$pizza->name}}"><img src="{{asset("frontend/images/pizzas/$pizza->image")}}" width="313px" height="220px" alt="1" />
                                                            </div>
                                                            <div class="clear"></div>
                                                            <div class="portfolio-items-single-description-wrapper">
                                                                <div class="portfolio-items-single-icon"><img src="{{asset('frontend/images/pizza-slice-black.png')}}" /></div>
                                                                <div class="portfolio-items-single-description">
                                                                    <a class="portfolio-items-single-title" href="orden.html" title="{{$pizza->name}}">{{$pizza->name}}</a>
                                                                    <div class="portfolio-items-single-content">
                                                                        <p>{{$pizza->description}}</p>
                                                                    </div>
                                                                    @foreach($pizza->pizza_prices as $price)
                                                                    <div class="portfolio-items-single-details">

                                                                        <div class="portfolio-detail">
                                                                            <div class="portfolio-detail-caption">{{$price->size}}</div>
                                                                            <div class="portfolio-detail-value">{{$price->price}} Bs.S</div>
                                                                        </div>
                                                                    </div>
                                                                    @endforeach
                                                                        
                                                                    <a style="margin-top: 35px" href="#" data-target="#modal-orden-{{$pizza->id}}"  data-toggle="modal" class="btn-add mt-4" data-price="25.99">Añadir a la carta</a>
                                                                    @include('frontend.partials.makeOrder')
                                                                </div>
                                                                <div class="clear"></div>
                                                            </div>
                                                        </div>



                                                          @endforeach
                                                    </div>
                                                    <div class="portfolio-item-separator portfolio-item-separator3">

                                                        <div class="clear"></div>
                                                        <div class="post-separator"><img src="{{asset('frontend/images/separator.png')}}}}" alt=""></div>
                                                    </div>
                                                    @endforeach
                                                        

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

     @include('frontend.partials.cart')


@endsection

@push('styles')
    <link rel="stylesheet" href="{{asset('frontend/css/themes/odometer-theme-default.css')}}">

@endpush
@push('scripts')

    <script src="{{asset('frontend/js/odometer.min.js')}}"></script>
    <script>

        Array.prototype.sum = function (prop) {
            var total = 0
            for ( var i = 0, _len = this.length; i < _len; i++ ) {
                total += this[i][prop]
            }
            return total
        }

        let currentPizza = 0;
        let tp = {!! $toppings !!};
         let prices = {!! $prices !!}
        let subtotal = 0;
        let toppings = 0;
        let toppings_sel = [];
        let addTopping;
        function clearMakeOrder() {
            subtotal = 0;
            toppings = 0;
            toppings_sel = [];
        }
        function getToppingsTotal() {
            $total = toppings_sel.sum("total");
            return $total ? $total : 0;
            //console.log($total);
        }
        function updateOrder() {
            console.log('click');
            let priceid = parseInt($(`#size-${currentPizza} :selected`).val());
            console.log(priceid);
            if (priceid) {
                let pizza_price = prices.find(x => x.id === priceid);
                $(`#pizza_price-${currentPizza}`).html(pizza_price.price);
                $(`#toppings_price-${currentPizza}`).html(getToppingsTotal());
                subtotal = pizza_price.price + getToppingsTotal();
            }
            $(`#total-${currentPizza}`).html(subtotal);


        }
        addTopping = function() {


            let item = $(`#topping-select-${currentPizza} :selected`);
            let qty = $(`#cantidad-${currentPizza} :selected`).val();
            let id = parseInt(item.val());

            if (toppings_sel.length>=5) {
                toastr.error('Solo se permiten agregar 5 toppings');
                return;
            }
            let topping = tp.find(x => x.id === id);
            if (toppings_sel.find(x => x.topping_id === id)) {
                toastr.error('Ya este topping está agregado');
                return;
            }
            let topping_selected = {
                id: toppings,
                topping_id: id,
                qty,
                price: topping.price,
                total: qty*topping.price
            };
            toppings_sel.push(topping_selected);
            if (topping) {
                let template = `
                         <li class="list-group-item" id="topping-${currentPizza}-${toppings}" style="display:none;">
                            <div class="float-sm-left">
                                <button class="btn btn-danger" onclick="event.preventDefault(); delToppings(${toppings})"><i class="fa fa-remove"></i></button>

                                ${topping.name} x ${qty}
                            </div>
                            <div class="float-sm-right">
                                <div class="badge badge-danger">${topping.price} Bs.S</div>
                             </div>
                            <input type="hidden" value="${id}" name="toppingId[]">
                            <input type="hidden" value="${qty}" name="toppingQty[]">
                        </li>
                    `;
                updateOrder();

                $("#toppings-"+currentPizza).append(template);
                $(`#topping-${currentPizza}-${toppings}`).fadeIn(function(e) {
                    $(this).show();
                });
                toppings++;
            }else {
                toastr.error('Selecciona un topping');
            }
        }

        function delToppings(id) {


            let topping = $(`#topping-${currentPizza}-${id}`);
            toppings_sel = toppings_sel.filter(e => e.id !== id);
            updateOrder();

            topping.fadeOut(function(e) {
                $(this).remove();
            });
            toppings--;

        }
        $('.modal-orden').on('show.bs.modal', function (e) {
            currentPizza = $(e.target).data().id;


            updateOrder();
            // do something...
        });

        $('.modal-orden').on('hidden.bs.modal', function (e) {
            $(e.target).find('form').trigger('reset');
            $("#toppings-"+currentPizza).html('');
            currentPizza = null;
            clearMakeOrder();

            // do something...
        });

        $(".size").change(function() {
            updateOrder();
        });

        $(".make-order-form").submit((e) => {
            e.preventDefault();

            let priceid = parseInt($(`#size-${currentPizza} :selected`).val());
            if (priceid) {

                data = $(e.target).serialize();

                addToCart(currentPizza,1,subtotal,data,function() {
                    $(`#modal-orden-${currentPizza}`).modal('hide');
                });

            }else {
                toastr.error('Escoge un tamaño para la pizza');
            }


        });

    </script>
@endpush
