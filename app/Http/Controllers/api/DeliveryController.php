<?php

namespace App\Http\Controllers\api;

use App\Delivery;
use App\Deliveryman;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DeliveryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        //$orders = Delivery::where('users_id',Auth::user()->id)->get();
//        $orders = Auth::user()->
        $orders = \Auth::user()->deliveryman->deliveries;

        return ['success' => true, 'orders' => $orders];
    }

    public function getDeliveryman($id) {
//      return $request->all();
        $deliveryman =  Deliveryman::where('users_id',$id)->first();
        return  new \App\Http\Resources\Deliveryman($deliveryman);
    }
    public function setDeliverymanStatus(Request $request,$id) {
//      return $request->all();
        $deliveryman =  Deliveryman::where('users_id',$id)->first();
        $deliveryman->status = $request->status;
        $deliveryman->update();
        return  new \App\Http\Resources\Deliveryman($deliveryman);
    }
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Delivery  $delivery
     * @return \Illuminate\Http\Response
     */
    public function show(Delivery $delivery)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Delivery  $delivery
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Delivery $delivery)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Delivery  $delivery
     * @return \Illuminate\Http\Response
     */
    public function destroy(Delivery $delivery)
    {
        //
    }
}
