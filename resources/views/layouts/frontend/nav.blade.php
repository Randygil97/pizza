          <div id="header" class="header-wrapper" style="background-image: url({{asset('frontend/images/header.jpg')}}">
            <div class="header-search">
                <div class="search-wrap">
                    <div class="center">
                        <div class="serach-inside">
                            <form method="get" id="searchform" class="search-form" action="#">
                                <div>
                                    <input type="text" id="s" name="s" value="Type text and hit enter" onfocus="if(this.value=='Type text and hit enter')this.value='';" onblur="if(this.value=='')this.value='Type text and hit enter';" autocomplete="off">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
           <div class="center">
                    <div class="header-inside">
                        <div class="logo">
                            <a href="index.html" title="Randy's"><img id="logoImage" width="200px" height="70px" src="{{ asset('frontend/images/logo-retina.jpg') }}" alt="STEWEYS - Pizza" /><img id="logoImageRetina" src="{{ asset('frontend/upload/logo-retina.jpg') }}" alt="STEWEYS - Pizza" /></a>
                            <div class="clear"></div>
                        </div>
                        <div class="main-menu">
                            <div class="menu-main-nav-menu-container">
                                <ul id="menu-main-nav-menu" class="sf-menu">
                                    <li class="menu-item @active('/')"><a href="{{url('/')}}">Inicio</a></li>
                                    <li class="menu-item @active('pizzas')"><a href="{{url('pizzas')}}">Pizzas</a></li>

                                    @guest
                                            <li class="menu-item "><a href="#loginModal" data-toggle="modal" >Ingresar</a></li>
                                    @endguest
                                    @auth 
                                      <li class="menu-item @active('orders')"><a href="{{url('orders')}}">Ordenes</a></li>
                                    <li class="menu-item menu-item-has-children"><a href="menu.html">Cuenta</a>
                                        <ul class="sub-menu" style="">
                                            <li class="menu-item ">
                                               
                                                        
                                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                    @csrf
                                                </form>
                                                <p class="text-white"><a href="" onclick="event.preventDefault();
                                                            document.getElementById('logout-form').submit();">Cerrar Sesión</a></p>
                                            </li>
                                            
                                        </ul>
                                    </li>
                                    @endauth
                                    <li class="menu-item @active('checkout')""><a href="{{url('checkout')}}"><i class="fa fa-shopping-cart"></i></a></li>


                                    <!--li class="menu-item"><a href="contact.html">Contact</a></li-->
                                </ul>

                            </div>
                        </div>
                        <div class="search-icon">
                            <div class="icons icon-search"></div>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>  
            <div class="clear"></div>
        </div>


        
      