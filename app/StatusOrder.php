<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 10 Aug 2018 17:19:07 +0000.
 */

namespace App;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class StatusOrder
 * 
 * @property int $id
 * @property string $status
 * 
 * @property \Illuminate\Database\Eloquent\Collection $orders
 *
 * @package App
 */
class StatusOrder extends Eloquent
{
	public $timestamps = false;

	protected $fillable = [
	    'id',
		'status'
	];

	public function orders()
	{
		return $this->hasMany(\App\Order::class, 'status_orders_id');
	}
}
