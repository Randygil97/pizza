<?php

namespace App\Http\Controllers\api;

use App\Http\Resources\Pizzas;
use App\Pizza;
use App\PizzaPrice;
use App\PizzasCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PizzasController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {

//        $page = $request->page ? $request->page : 1;
        $per_page = $request->per_page ? $request->per_page : 5;

        $pizzas = Pizza::with('pizza_prices')->paginate($per_page);



        return Pizzas::collection($pizzas);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){

        $this->validate($request,[
            'name' => 'required|string|min:6|max:30',
            'description' => 'required|string|min:20|max:300',
            'image' => 'required|image'

        ]);
        $pizza = new Pizza();

        $pizza->fill($request->only('name','description'));
        $pizza->save();

        if($request->hasfile('image'))  {
            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension(); // getting image extension
            $filename = "pizza-".time().'.'.$extension;
            $dir = 'frontend/images/pizzas';
            $file->move($dir, $filename);
            \File::delete($dir.DIRECTORY_SEPARATOR.$pizza->image);
            $pizza->image = $filename;
        }
        $pizza->update();
        return ['success' => true];


    }


    public function storePrice(Request $request,Pizza $pizza)
    {

        $this->validate($request, [
            'size' => 'required|string',
            'price' => 'required|integer'

        ]);

        $sprice = PizzaPrice::where([
            ['pizza_id',$pizza->id],
            ['size',$pizza->size],
        ])->count();

        if ($sprice>0) {
            return ['success' => true,'error' => 'Ese tamaño ya está agregado'];
        }
        $price = PizzaPrice::create([
            'size' => $request->size,
            'price' => $request->price,
            'pizza_id' => $request->pizza_id
        ]);
        $price->save();

        return ['success' => true,'price' => $price];

    }
    public function updatePrice(Request $request,Pizza $pizza,PizzaPrice $price)
    {

        $this->validate($request, [
            'size' => 'required|string',
            'price' => 'required|integer'

        ]);


        //$price->fill($request->all());
        $price->size = $request->size;
        $price->price = $request->price;
        $price->update();

        return ['success' => true,'price' => $price];

    }
    public function deletePrice(PizzaPrice $price) {
        $price->delete();

        return ['success' => true];

    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Pizza  $pizza
     * @return \Illuminate\Http\Response
     */
    public function show(Pizza $pizza)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Pizza  $pizza
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Pizza $pizza) {
        $this->validate($request,[
            'name' => 'required|string|min:6|max:30',
            'description' => 'required|string|min:20|max:300',
            'image' => 'image'

        ]);

        if($request->hasfile('image'))  {
            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension(); // getting image extension
            $filename = "pizza-".time().'.'.$extension;
            $dir = 'frontend/images/pizzas';
            $file->move($dir, $filename);
            \File::delete($dir.DIRECTORY_SEPARATOR.$pizza->image);
            $pizza->image = $filename;
        }
        $pizza->fill($request->only('name','description'));
        $pizza->update();
        return ['success' => true];

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Pizza  $pizza
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pizza $pizza)
    {
        $pizza->delete();

        return ['success' => true];
    }
}
