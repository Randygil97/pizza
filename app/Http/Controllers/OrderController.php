<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Order;
class OrderController extends Controller
{
	public function __construct() {
		$this->middleware('auth');
		
	}
    public function index() {

    	$orders = Order::with([
            'status_order',
            'status_order',

            'orderPizzas',
            'orderPizzas.orders_pizza_toppings',
            'orderPizzas.orders_pizza_toppings.pizzas_topping',
            'delivery',
            'delivery.status_delivery',
            'payments',
            'payments.payment_method'
            
        ])->where('users_id',Auth::user()->id)->get();

        return view('frontend.orders',compact('orders'));

    }
}
