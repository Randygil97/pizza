<div class="modal fade modal-orden" data-id="{{$pizza->id}}" id="modal-orden-{{$pizza->id}}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" >
            <form action="{{url('/cart/add')}}" method="post" class="make-order-form">
                <div class="modal-header">
                    <p>{{$pizza->name}}</p>
                </div>
                <div class="modal-body">
                    <div class="row justify-content-center">
                            <img  src="{{asset("frontend/images/pizzas/$pizza->image")}}" class="img-fluid" alt="" style="width: 200px; height: 200px">
                    </div>
                    <h1 class="text-center ">{{$pizza->name}}</h1>
                    <h3>Personalizar Pizza</h3>
                    <div class="row">
                        <div class="col-sm-3 col-xs-12">
                            <div class="form-group">
                                <label for="size">Tamaño</label>

                                {{ Form::select('size', $pizza->pizza_prices->pluck('size','id'), null, ['placeholder' => 'Escoge un tamaño','id'=>'size-'.$pizza->id,'class' => 'form-control size']) }}


                            </div>
                        </div>
                        <div class="col-sm-9 col-xs-12">
                            <div class="form-group">
                                <label for="">Preparación Especial</label>
                                <textarea class="form-control" name="special_preparation" placeholder="Ej. Bordes tostados"></textarea>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-sm-12 ">
                            <h3>Toppings</h3>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-5">
                            <div class="form-group">
                                <label for="toppings">Agregar</label>
                                {{ Form::select('topping-select', $toppings->pluck('name','id'), null, ['placeholder' => 'Escoge un topping','id'=>'topping-select-'.$pizza->id,'class' => 'form-control']) }}

                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-5">
                            <div class="form-group">
                                <label for="cantidad">Cantidad</label>
                                <select class="form-control mb-3" id="cantidad-{{$pizza->id}}">
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    </select>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-2">
                            <div class="form-group">
                            <a class="btn btn-danger text-white" style="margin-top:25px;" onclick="addTopping()">Agregar</a></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 col-xs-12">
                            <ul class="list-group" id="toppings-{{$pizza->id}}">

                            </ul>
                        </div>
                    </div>
                    <div class="row mt-5">
                        <div class="col text-right">
                            <h4><strong>Precio de la pizza:</strong> <span id="pizza_price-{{$pizza->id}}" class="odometer"></span> Bs.S</h4>
                            <h4><strong>Precio de los toppings:</strong> <span id="toppings_price-{{$pizza->id}}" class="odometer"></span> Bs.S </h4>

                            <h4><strong>Total:</strong> <span id="total-{{$pizza->id}}" class="odometer"></span> Bs.S</h4>
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col text-right">

                            <button type="submit" class="btn btn-danger white-text">Realizar Orden</button>
                        </div>

                    </div>
                </div>
            </form>
        </div>

    </div>
</div>

@push('style')
<style>
  .modal-lg {
    max-width: 80% !important;
}
</style>
@endpush
@push('scripts')


@endpush