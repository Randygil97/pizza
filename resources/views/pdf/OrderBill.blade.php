<!doctype html>
<html>
<head>
    <title>Factura Fiscal</title>


</head>

<body>
<div class="invoice-box">
    <table cellpadding="0" cellspacing="0">
        <tr class="top">
            <td colspan="2">
                <table>
                    <tr>
                        <td class="title">
                            <img src="{{asset('frontend/images/logo-retina.jpg')}}" style="width:100%; max-width:150px; max-height:150px;">
                        </td>

                        <td>
                            Orden #{{$order->id}}<br>
                            Fecha: {{\Carbon\Carbon::parse($order->created_at)->toDateTimeString()}}<br>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>

        <tr class="information">
            <td colspan="2">
                <table>
                    <tr>
                        <td>
                            Dirección Fiscal: <br>
                            Prolongación Avenida Bolivar,<br>
                            Parroquia San Juan de Los Morros<br>
                            Centro Comercial Paseo San Juan, Local #4
                        </td>

                        <td>
                            Pizzeria Randy's C.A<br>
                            Rif: J-26428919-6<br>
                            Telf: 0412-3461587
                        </td>
                    </tr>
                </table>
            </td>
        </tr>

        <tr class="heading">
            <td>
                Método de Pago
            </td>

            <td>
                Referencia
            </td>
        </tr>

        <tr class="details">
            <td>
                {{$order->payments->payment_method->name}}
            </td>

            <td>
               @if($order->payments->payment_method->id==2)  {{$order->payments->reference}} @endif
            </td>
        </tr>

        <tr class="heading">
            <td>
                Artículo
            </td>

            <td>
                Precio
            </td>
        </tr>
        @foreach($order->orderPizzas as $pizza)
            <tr class="item">
                <td>
                    {{$pizza->pizza->name}}
                </td>

                <td>
                    {{$pizza->price}} Bs.S
                </td>
            </tr>
        @endforeach


        <tr class="total">
            <td></td>

            <td>
                IVA ({{$order->iva}}%): {{$order->iva/100*$order->total}} Bs.S
            </td>
        </tr>
        <tr class="total">
            <td></td>

            <td>
                SubTotal: {{$order->total}} Bs.S
            </td>
        </tr>
        <tr class="total">
            <td></td>

            <td>
                Total: {{$order->iva/100*$order->total+$order->total}} Bs.S
            </td>
        </tr>
    </table>
</div>
</body>
</html>