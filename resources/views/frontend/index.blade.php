@extends('layouts.frontend.app')

@section('content')
 <div class="container-wrapper">
    <div id="container" class="home-template">
                <!-- start container -->
               
                
            
       <div class="center">
            <div class="vc_row wpb_row vc_row-fluid">
                <div class="wpb_column vc_column_container vc_col-sm-12">
                    <div class="vc_column-inner ">
                        <div class="wpb_wrapper">
                            <div id="rev_slider_3_1_wrapper" class="rev_slider_wrapper fullscreen-container" style="background-color:transparent;padding:0px;">
                                <!-- START REVOLUTION SLIDER 5.2.3.5 fullscreen mode -->
                                <div id="rev_slider_3_1" class="rev_slider fullscreenbanner" style="display:none;" data-version="5.2.3.5">
                                    <ul>
                                        <!-- SLIDE  -->
                                        <li data-index="rs-11" data-transition="zoomin" data-slotamount="7" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="2000" data-thumb="upload/5-100x50.jpg" data-rotate="0" data-saveperformance="off" data-title="First" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                                            <!-- MAIN IMAGE -->
                                            <img src="{{ asset('frontend/images/slider/slider-1.jpg') }}" alt="" title="5" width="1920" height="1080" data-lazyload="{{ asset('frontend/images/slider/slider-1.jpg') }}" data-bgposition="center center" data-kenburns="on" data-duration="30000" data-ease="Linear.easeNone" data-scalestart="100" data-scaleend="120" data-rotatestart="0" data-rotateend="0" data-offsetstart="0 0" data-offsetend="0 0" data-bgparallax="10" class="rev-slidebg" data-no-retina>
                                            <!-- LAYERS -->
                                        </li>
                                        <!-- SLIDE  -->
                                        <li data-index="rs-12" data-transition="zoomin" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="upload/from-farm-100x50.jpg" data-rotate="0" data-saveperformance="off" data-title="Second" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                                            <!-- MAIN IMAGE -->
                                            <img src="{{ asset('frontend/images/slider/slider-2.jpg') }}" alt="" title="from-farm" width="1920" height="1080" data-lazyload="{{ asset('frontend/images/slider/slider-2.jpg') }}" data-bgposition="center center" data-kenburns="on" data-duration="30000" data-ease="Linear.easeNone" data-scalestart="100" data-scaleend="120" data-rotatestart="0" data-rotateend="0" data-offsetstart="0 0" data-offsetend="0 0" data-bgparallax="10" class="rev-slidebg" data-no-retina>
                                            <!-- LAYERS -->
                                        </li>
                                        <!-- SLIDE  -->
                                        <li data-index="rs-13" data-transition="zoomin" data-slotamount="7" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="2000" data-thumb="upload/classic-100x50.jpg" data-rotate="0" data-saveperformance="off" data-title="Third" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                                            <!-- MAIN IMAGE -->
                                            <img src="{{ asset('frontend/images/slider/slider-3.jpg') }}" alt="" title="classic" width="1920" height="1080" data-lazyload="{{ asset('frontend/images/slider/slider-3.jpg') }}" data-bgposition="center center" data-kenburns="on" data-duration="30000" data-ease="Linear.easeNone" data-scalestart="100" data-scaleend="120" data-rotatestart="0" data-rotateend="0" data-offsetstart="0 0" data-offsetend="0 0" data-bgparallax="10" class="rev-slidebg" data-no-retina>
                                            <!-- LAYERS -->
                                        </li>
                                    </ul>
                                    <div style="" class="tp-static-layers">

                                        <!-- LAYER NR. 1 -->
                                        <!--div class="tp-caption   tp-resizeme tp-static-layer" id="slider-3-layer-1" data-x="['left','left','left','left']" data-hoffset="['135','135','135','135']" data-y="['top','top','top','top']" data-voffset="['119','119','119','119']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="opacity:0;s:300;e:Power2.easeInOut;" data-transform_out="opacity:0;s:300;s:300;" data-start="500" data-splitin="none" data-splitout="none" data-responsive_offset="on" data-startslide="0" data-endslide="2" style="z-index: 5; white-space: nowrap; font-size: 20px; line-height: 22px; font-weight: 400; color: rgba(255, 255, 255, 1.00);font-family:'Oswald', sans-serif;text-transform:uppercase;font-size:58px;color:#fff;text-shadow:0px 2px 2px rgba(0, 0, 0, 0.8);letter-spacing:-1px;font-weight:700;">Pide tu </div-->

                                        <!-- LAYER NR. 2 -->
                                        <div class="tp-caption   tp-resizeme tp-static-layer" id="slider-3-layer-2" data-x="['left','left','left','left']" data-hoffset="['233','233','233','233']" data-y="['top','top','top','top']" data-voffset="['189','189','189','189']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="opacity:0;s:300;e:Power2.easeInOut;" data-transform_out="opacity:0;s:300;s:300;" data-start="500" data-splitin="none" data-splitout="none" data-responsive_offset="on" data-startslide="0" data-endslide="2" style="z-index: 6; white-space: nowrap; font-size: 20px; line-height: 22px; font-weight: 400; color: rgba(255, 255, 255, 1.00);font-family:'Oswald', sans-serif;text-transform:uppercase;font-size:100px;color:#fff;text-shadow:0px 2px 2px rgba(0, 0, 0, 0.8);letter-spacing:-1px;font-weight:700;">Pide tu </div>

                                        <!-- LAYER NR. 3 -->
                                        <div class="tp-caption   tp-resizeme tp-static-layer" id="slider-3-layer-4" data-x="['left','left','left','left']" data-hoffset="['606','606','606','606']" data-y="['top','top','top','top']" data-voffset="['468','468','468','468']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="opacity:0;s:300;e:Power2.easeInOut;" data-transform_out="opacity:0;s:300;s:300;" data-start="500" data-splitin="none" data-splitout="none" data-responsive_offset="on" data-startslide="0" data-endslide="2" style="z-index: 7; white-space: nowrap; font-size: 20px; line-height: 22px; font-weight: 400; color: rgba(255, 255, 255, 1.00);font-family:'Oswald', sans-serif;text-transform:uppercase;font-size:58px;color:#fff;text-shadow:0px 2px 2px rgba(0, 0, 0, 0.8);letter-spacing:-1px;font-weight:700;">Donde quiera que estés </div>

                                        <!-- LAYER NR. 4 -->
                                        <div class="tp-caption   tp-resizeme tp-static-layer" id="slider-3-layer-5" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['top','top','top','top']" data-voffset="['249','249','249','249']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="opacity:0;s:300;e:Power2.easeInOut;" data-transform_out="opacity:0;s:300;s:300;" data-start="500" data-splitin="none" data-splitout="none" data-responsive_offset="on" data-startslide="0" data-endslide="2" style="z-index: 8; white-space: nowrap; font-size: 20px; line-height: 22px; font-weight: 400; color: rgba(255, 255, 255, 1.00);font-family:'Oswald', sans-serif;text-transform:uppercase;padding:10px 30px;font-size:170px;color:#fff;text-shadow:0px 2px 2px rgba(0, 0, 0, 0.8);letter-spacing:-1px;font-weight:700;background:#d8252f;line-height:1;">PIZZA </div>
                                    </div>

                                    <div class="tp-bannertimer tp-bottom" style="height: 3px; background-color: rgba(255, 255, 255, 0.25);"></div>
                                </div>

                            </div>
                            
        
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end container -->
    <div class="clear"></div>
</div>
@endsection