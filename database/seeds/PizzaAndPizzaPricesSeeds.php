<?php

use Illuminate\Database\Seeder;
use App\Pizza;
use App\PizzaPrice;
use App\PizzasTopping;
class PizzaAndPizzaPricesSeeds extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
//        \DB::table('pizzas')->truncate();
//        \DB::table('pizza_prices')->truncate();
//        \DB::table('pizza_toppings')->truncate();
        $bas = \App\PizzasCategory::create([
            'name' => 'Básicas',
            'description' => 'Pizzas Básicas'
        ]);

        $veg = \App\PizzasCategory::create([
            'name' => 'Vegetarianas',
            'description' => 'Pizzas Vegetarianas'
        ]);
        $margarita = Pizza::create([
            'name' => 'Pizza Margarita',
            'description' => 'Peperoni, Queso, Olivas negras, Salsa de Tomate, Hongos, Carne, Cebolla, Ensalada',
            'image' => 'pizza-1.jpg',
            'pizzas_categories_id' => $bas->id
        ]);
        $napolitana = Pizza::create([
            'name' => 'Pizza Napolitana',
            'description' => 'Tomate, oregano, ajo, aceite de oliva extra virgen, peperoni, queso, cebolla roja, salsa y hongos.',
            'image' => 'pizza-6.jpg',
            'pizzas_categories_id' => $bas->id
        ]);

        $berenjena = Pizza::create([
            'name' => 'Berenjena y Calabacin',
            'description' => 'Tomate frito, Calabacín, Berenjena,Queso Mozzarella, Queso rallado',
            'image' => 'pizza-4.jpg',
            'pizzas_categories_id' => $veg->id
        ]);
        PizzaPrice::create([
            'price' => 8.30,
            'size' => 'Pequeña',
            'pizza_id' => $margarita->id
        ]);
        PizzaPrice::create([
            'price' => 12,
            'size' => 'Mediana',
            'pizza_id' => $margarita->id
        ]);

        PizzaPrice::create([
            'price' => 14.60,
            'size' => 'Grande',
            'pizza_id' => $margarita->id
        ]);
        PizzaPrice::create([
            'price' => 8.30,
            'size' => 'Pequeña',
            'pizza_id' => $napolitana->id
        ]);
        PizzaPrice::create([
            'price' => 12,
            'size' => 'Mediana',
            'pizza_id' => $napolitana->id
        ]);

        PizzaPrice::create([
            'price' => 14.60,
            'size' => 'Grande',
            'pizza_id' => $napolitana->id
        ]);
        PizzaPrice::create([
            'price' => 12.30,
            'size' => 'Pequeña',
            'pizza_id' => $berenjena->id
        ]);
        PizzaPrice::create([
            'price' => 15,
            'size' => 'Mediana',
            'pizza_id' => $berenjena->id
        ]);

        PizzaPrice::create([
            'price' => 18.60,
            'size' => 'Grande',
            'pizza_id' => $berenjena->id
        ]);
        PizzasTopping::create([
            'name' => 'Champiñones',
            'price' => 3,
            'description' => '50g de Champiñones picados'
        ]);
        PizzasTopping::create([
            'name' => 'Salchichón',
            'price' => 5,
            'description' => '30g de Champiñones picados'
        ]);

        PizzasTopping::create([
            'name' => 'Maíz',
            'price' => 5,
            'description' => '20g de Champiñones picados'
        ]);

        PizzasTopping::create([
            'name' => 'Tocineta',
            'price' => 7,
            'description' => '30g de Champiñones picados'
        ]);

        PizzasTopping::create([
            'name' => 'Maíz',
            'price' => 5,
            'description' => '20g de Champiñones picados'
        ]);
    }
}
