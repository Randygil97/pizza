<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToOrdersPizzaToppingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('orders_pizza_toppings', function(Blueprint $table)
		{
			$table->foreign('pizza_toppings_id', 'fk_orders_pizza_toppings')->references('id')->on('pizzas_toppings')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('orders_pizza_id', 'fk_toppings_orders_pizza_id')->references('id')->on('orders_pizza')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('orders_pizza_toppings', function(Blueprint $table)
		{
			$table->dropForeign('fk_orders_pizza_toppings');
			$table->dropForeign('fk_toppings_orders_pizza_id');
		});
	}

}
