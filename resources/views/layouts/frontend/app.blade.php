<!DOCTYPE html>
<html lang="en-US">


<head>
    <meta name="referrer" content="no-referrer"> <!-- don't send HTTP referer for privacy purpose and to use Google Maps Geocoding API without key -->

    <meta charset="UTF-8">
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->

    <!-- for mobile devices -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta name="format-detection" content="telephone=no" />
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Favicon Icon -->
    <link rel="shortcut icon" href="{{ asset('frontend/images/favicon.ico') }}" type="image/vnd.microsoft.icon" />
    <link rel="icon" href="{{ asset('frontend/images/favicon.ico') }}" type="image/x-ico" />

    <title>Randy's | Pizza a domicilio</title>
       <link rel="stylesheet" href="{{asset('frontend/css/toastr.min.css')}}">
    <link rel="stylesheet" href="{{asset('frontend/css/reset.css')}}">

    <link rel="stylesheet" href="{{asset('frontend/css/cart.css')}}">
    <link rel="stylesheet" href="{{ asset('frontend/css/bootstrap/bootstrap.min.css') }}">
    <link rel='stylesheet' href="{{ asset('frontend/css/settings.css') }}" type='text/css' media='all' />
    <link rel='stylesheet' href="{{ asset('frontend/css/animsition.css') }}" type='text/css' media='all' />
    <link rel='stylesheet' href="{{ asset('frontend/css/fontello.css') }}" type='text/css' media='all' />
    <link rel='stylesheet' href="{{ asset('frontend/css/font-awesome.css') }}" type='text/css' media='all' />
    <link rel='stylesheet' href="{{ asset('frontend/css/pe-icon-7-stroke.css') }}" type='text/css' media='all' />
    <link rel='stylesheet' href="{{ asset('frontend/css/prettyPhoto.css') }}" type='text/css' media='all' />
    <link rel='stylesheet' href="{{ asset('frontend/css/style.css') }}" type='text/css' media='all' />
    <link rel='stylesheet' href="{{ asset('frontend/css/media.css') }}" type='text/css' media='all' />
    <link rel='stylesheet' href="{{ asset('frontend/css/js_composer.min.css') }}" type='text/css' media='all' />
@stack('styles')
</head>
<body class="@if(Route::currentRouteName()=='index')  page-template page-template-template-home page-template-template-home-php  @else page page-template-default  @endif wpb-js-composer js-comp-ver-4.11.1 vc_responsive">
    <div class="animsition global-wrapper">

    
        @include('layouts.frontend.nav')
          
        @yield('content')



        <!-- end container-wrapper -->
        <div id="footer" class="footer">
            <div class="center">
                <div class="footer-left">
                   Todos los derechos e izquierdos reservados &copy 2018 </div>
                <div class="footer-right">
                    <ul class="socials-sh">
                        <li>
                            <a class="sh-socials-url icon-facebook" href="#" title="My Facebook profile" target="_blank"></a>
                        </li>
                        <li>
                            <a class="sh-socials-url icon-instagram" href="#" title="Instagram" target="_blank"></a>
                        </li>
                        <li>
                            <a class="sh-socials-url icon-pinterest" href="#" title="Pinterest" target="_blank"></a>
                        </li>
                    </ul>
                </div>
                <div class="clear"></div>
            </div>
        </div>
    @include('layouts.frontend.partials.login')

    </div>



    <script src="{{ asset('frontend/js/jquery.min.js') }} "></script>
    <script src="{{ asset('frontend/js/main.js') }}"></script>
    <script src="{{ asset('frontend/js/jquery/jquery-migrate.min.js') }} "></script>
    <script src="{{ asset('frontend/js/bootstrap.min.js') }} "></script>
    <script src="{{ asset('frontend/js/jquery.themepunch.tools.min.js') }} "></script>
    <script src="{{ asset('frontend/js/jquery.themepunch.revolution.min.js') }} "></script>
    <script src="{{asset('frontend/js/extensions/revolution.extension.slideanims.min.js') }}"></script>
    <script src="{{asset('frontend/js/extensions/revolution.extension.layeranimation.min.js') }}"></script>
    <script src="{{asset('frontend/js/extensions/revolution.extension.navigation.min.js') }}"></script>
    <script src="{{asset('frontend/js/extensions/revolution.extension.actions.min.js') }}"></script>
    <script src="{{asset('frontend/js/extensions/revolution.extension.carousel.min.js') }}"></script>
    <script src="{{asset('frontend/js/extensions/revolution.extension.kenburn.min.js') }}"></script>
    <script src="{{asset('frontend/js/extensions/revolution.extension.migration.min.js') }}"></script>
    <script src="{{asset('frontend/js/extensions/revolution.extension.parallax.min.js') }}"></script>
    <script src="{{asset('frontend/js/extensions/revolution.extension.video.min.js') }}"></script>


    <script>
        /******************************************
        				-	PREPARE PLACEHOLDER FOR SLIDER	-
        			******************************************/

        var setREVStartSize = function() {
            try {
                var e = new Object,
                    i = jQuery(window).width(),
                    t = 9999,
                    r = 0,
                    n = 0,
                    l = 0,
                    f = 0,
                    s = 0,
                    h = 0;
                e.c = jQuery('#rev_slider_3_1');
                e.responsiveLevels = [1240, 1024, 1024, 1024];
                e.gridwidth = [1240, 1024, 778, 480];
                e.gridheight = [600, 600, 500, 400];

                e.sliderLayout = "fullscreen";
                e.fullScreenAutoWidth = 'off';
                e.fullScreenAlignForce = 'off';
                e.fullScreenOffsetContainer = '.footer';
                e.fullScreenOffset = '';
                e.minHeight = 700;
                if (e.responsiveLevels && (jQuery.each(e.responsiveLevels, function(e, f) {
                        f > i && (t = r = f, l = e), i > f && f > r && (r = f, n = e)
                    }), t > r && (l = n)), f = e.gridheight[l] || e.gridheight[0] || e.gridheight, s = e.gridwidth[l] || e.gridwidth[0] || e.gridwidth, h = i / s, h = h > 1 ? 1 : h, f = Math.round(h * f), "fullscreen" == e.sliderLayout) {
                    var u = (e.c.width(), jQuery(window).height());
                    if (void 0 != e.fullScreenOffsetContainer) {
                        var c = e.fullScreenOffsetContainer.split(",");
                        if (c) jQuery.each(c, function(e, i) {
                            u = jQuery(i).length > 0 ? u - jQuery(i).outerHeight(!0) : u
                        }), e.fullScreenOffset.split("%").length > 1 && void 0 != e.fullScreenOffset && e.fullScreenOffset.length > 0 ? u -= jQuery(window).height() * parseInt(e.fullScreenOffset, 0) / 100 : void 0 != e.fullScreenOffset && e.fullScreenOffset.length > 0 && (u -= parseInt(e.fullScreenOffset, 0))
                    }
                    f = u
                } else void 0 != e.minHeight && f < e.minHeight && (f = e.minHeight);
                e.c.closest(".rev_slider_wrapper").css({
                    height: f
                })

            } catch (d) {
                console.log("Failure at Presize of Slider:" + d)
            }
        };

        setREVStartSize();

        var tpj = jQuery;

        var revapi3;
        tpj(document).ready(function() {
            if (tpj("#rev_slider_3_1").revolution == undefined) {
                revslider_showDoubleJqueryError("#rev_slider_3_1");
            } else {
                revapi3 = tpj("#rev_slider_3_1").show().revolution({
                    sliderType: "standard",
                    jsFileLocation: "//pegodesign.com/wp-themes/steweys/js/",
                    sliderLayout: "fullscreen",
                    dottedOverlay: "none",
                    delay: 5000,
                    navigation: {
                        keyboardNavigation: "off",
                        keyboard_direction: "horizontal",
                        mouseScrollNavigation: "off",
                        mouseScrollReverse: "default",
                        onHoverStop: "off",
                        touch: {
                            touchenabled: "on",
                            swipe_threshold: 75,
                            swipe_min_touches: 50,
                            swipe_direction: "horizontal",
                            drag_block_vertical: false
                        },
                        arrows: {
                            style: "hesperiden",
                            enable: true,
                            hide_onmobile: true,
                            hide_under: 600,
                            hide_onleave: true,
                            hide_delay: 200,
                            hide_delay_mobile: 1200,
                            tmp: '',
                            left: {
                                h_align: "left",
                                v_align: "center",
                                h_offset: 30,
                                v_offset: 0
                            },
                            right: {
                                h_align: "right",
                                v_align: "center",
                                h_offset: 30,
                                v_offset: 0
                            }
                        },
                        bullets: {
                            enable: true,
                            hide_onmobile: true,
                            hide_under: 600,
                            style: "hephaistos",
                            hide_onleave: true,
                            hide_delay: 200,
                            hide_delay_mobile: 1200,
                            direction: "horizontal",
                            h_align: "center",
                            v_align: "bottom",
                            h_offset: 0,
                            v_offset: 30,
                            space: 5,
                            tmp: ''
                        }
                    },
                    viewPort: {
                        enable: true,
                        outof: "wait",
                        visible_area: "80%"
                    },
                    responsiveLevels: [1240, 1024, 1024, 1024],
                    visibilityLevels: [1240, 1024, 1024, 1024],
                    gridwidth: [1240, 1024, 778, 480],
                    gridheight: [600, 600, 500, 400],
                    lazyType: "smart",
                    minHeight: 700,
                    parallax: {
                        type: "mouse",
                        origo: "slidercenter",
                        speed: 2000,
                        levels: [2, 3, 4, 5, 6, 7, 12, 16, 10, 50, 47, 48, 49, 50, 51, 55],
                        type: "mouse",
                    },
                    shadow: 0,
                    spinner: "off",
                    stopLoop: "off",
                    stopAfterLoops: -1,
                    stopAtSlide: -1,
                    shuffle: "off",
                    autoHeight: "off",
                    fullScreenAutoWidth: "off",
                    fullScreenAlignForce: "off",
                    fullScreenOffsetContainer: ".footer",
                    fullScreenOffset: "",
                    hideThumbsOnMobile: "off",
                    hideSliderAtLimit: 0,
                    hideCaptionAtLimit: 0,
                    hideAllCaptionAtLilmit: 0,
                    debugMode: false,
                    fallbacks: {
                        simplifyAll: "off",
                        nextSlideOnWindowFocus: "off",
                        disableFocusListener: false,
                    }
                });
            }
        }); /*ready*/
    </script>


    <script src="{{ asset('frontend/js/modernizr.custom.js') }} "></script>
    <script src="{{ asset('frontend/js/jquery.animsition.min.js') }} "></script>
    <script src="{{ asset('frontend/js/superfish.js') }} "></script>
    <script src="{{ asset('frontend/js/jquery.mobilemenu.js') }} "></script>
    <script src="{{ asset('frontend/js/custom.js') }} "></script>
    <script src="{{ asset('frontend/js/js_composer_front.min.js') }} "></script>
    <script src="{{asset('frontend/js/toastr.min.js')}}"></script>
    <script src="{{asset('frontend/js/axios.min.js')}}"></script>
    <script>
        let pizzas;
        axios.get('{{url('pizzasGet')}}').then((e) => { pizzas = e.data });
    </script>
    <script>
        toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-bottom-full-width",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
        }
        axios.defaults.headers.common = {
            'X-Requested-With': 'XMLHttpRequest',
            'X-CSRF-TOKEN' : document.querySelector('meta[name="csrf-token"]').getAttribute('content')
        };
    </script>
    @auth
        <script src="{{asset('frontend/js/echo.js')}}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pusher/4.3.1/pusher.min.js"></script>
        <script>
            let userId = {{Auth::user()->id}};
            window.Echo = new Echo({
                broadcaster: 'pusher',
                key: '9a954494eaf2e18050be',
                cluster: 'us2',
                encrypted: true,
                auth: {
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                }
            },
              authEndpoint: '/guard/broadcast/auth',

            });
            window.Echo.connector.pusher.connection.bind('connected', (event) => console.log(event))
            window.Echo.connector.pusher.connection.bind('disconnected', () => console.log('disconnect'))


            // window.Echo.private('pizza-tracker')
            //     .listen('OrderStatusChanged', (order) => {
            //         console.log(order);
            //         if (id === order.users_id) {
            //             console.log('WEEEPA');
            //             // this.notifications.unshift({
            //             //     description: 'Order ID: ' + order.id + ' updated',
            //             //     url: '/orders/' + order.id,
            //             //     time: new Date()
            //             // })
            //         }
            //     });
        </script>
    @endauth
    @stack('scripts')

</body>
</html>