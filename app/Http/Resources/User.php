<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class User extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
          'id' => $this->id,
          'username' => $this->username,
          'persons_id' => $this->persons_id,
          'email' => $this->email,
           'roles' =>  Roles::collection($this->whenLoaded('roles')),
          'person' => new Person($this->whenLoaded('person'))

        ];
    }
}
