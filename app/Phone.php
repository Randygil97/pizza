<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 10 Aug 2018 17:19:07 +0000.
 */

namespace App;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Phone
 * 
 * @property int $id
 * @property string $number
 * @property int $persons_id
 * 
 * @property \App\Person $person
 *
 * @package App
 */
class Phone extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'persons_id' => 'int'
	];

	protected $fillable = [
		'number',
		'persons_id'
	];

	public function person()
	{
		return $this->belongsTo(\App\Person::class, 'persons_id');
	}
}
