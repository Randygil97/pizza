<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 03 Sep 2018 19:58:42 +0000.
 */

namespace App;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Delivery
 * 
 * @property int $id
 * @property int $users_id
 * @property int $orders_id
 * @property int $status_deliveries_id
 * @property int $deliverymen_id
 * 
 * @property \App\Deliveryman $deliveryman
 * @property \App\Order $order
 * @property \App\StatusDelivery $status_delivery
 *
 * @package App
 */
class Delivery extends Eloquent
{
	public $timestamps = true;

	protected $casts = [
		'orders_id' => 'int',
		'status_deliveries_id' => 'int',
		'deliverymen_id' => 'int'
	];
	protected $fillable = [
		'deliverymen_id',
		'orders_id',
		'status_deliveries_id',
		'delivered_time'
	];

	public function getordertotalAttribute() {
		return ($this->iva/100*$this->total)+$this->total;
	}

	public function deliveryman()
	{
		return $this->belongsTo(\App\Deliveryman::class, 'deliverymen_id');
	}

	public function order()
	{
		return $this->belongsTo(\App\Order::class, 'orders_id');
	}

	public function status_delivery()
	{
		return $this->belongsTo(\App\StatusDelivery::class, 'status_deliveries_id');
	}
}
