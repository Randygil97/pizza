import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Login from './views/Login.vue'
import Register from './views/Register.vue'
import Pizzas from './views/pizza/Pizzas.vue'
import Category from './views/pizza/Category.vue'
import Users from './views/user/List.vue'
import Orders from './views/orders/Orders.vue'
import Repartidor from './views/repartidor/Orders'
import Toppings from './views/toppings/Index'
import Settings from './views/Settings.vue'

import NProgress from 'nprogress';

Vue.use(Router)

let router = new Router({
    routes: [
        {
            path: '/',
            name: 'admin',
            component: Home,
            alias: '/admin',
            meta: {
                auth: ['admin','repartidor','empleado'],
                forbiddenRedirect: '/404',
                authRedirect: '/login'

            }
        },
        /*{
            path: '/admin',
            name: 'home',
            component: Home,
            // meta: {auth: {roles: 'admin', redirect: {name: 'default'}, forbiddenRedirect: '/403'}},
            meta: {auth:true},
            */
        {
            path: '/pizzas',
            name: 'pizzas',
            component: Pizzas,
            meta: {
                auth: ['admin']
            }
        },
         {
            path: '/categories',
            name: 'categories',
            component: Category,
            meta: {
                auth: ['admin']
            }
        },
        {
            path: '/users',
            name: 'users',
            component: Users,
            meta: {
                auth: ['admin'],
                forbiddenRedirect: '/404'
            }
        }
        ,
        {
            path: '/orders',
            name: 'orders',
            component: Orders,
            meta: {
                auth: ['admin','empleado']
            }
        },

        ,{
            path: '/toppings',
            name: 'toppings',
            component: Toppings,
            meta: {
                auth: ['admin']
            }
        },
        {
            path: '/repartidor',
            name: 'repartidor',
            component: Repartidor,
            meta: {
                auth: ['repartidor']
            }
        },
        {
            path: '/login',
            name: 'login',
            component: Login
        },
        {
            path: '/register',
            name: 'register',
            component: Register
        },
        {
            path: '/settings',
            name: 'settings',
            component: Settings
        },
        {
            path: '/about',
            name: 'about',
            // route level code-splitting
            // this generates a separate chunk (about.[hash].js) for this route
            // which is lazy-loaded when the route is visited.
            component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
        }
    ]
})
//
router.beforeResolve((to, from, next) => {
    if (to.name) {
        NProgress.start()
    }
    next()
})

router.afterEach((to, from) => {
    NProgress.done()
})



export default router
