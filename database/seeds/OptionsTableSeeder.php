<?php

use Illuminate\Database\Seeder;
use App\Options;
class OptionsTableSeeder extends Seeder

{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Options::create([
        	'option' => 'iva',
        	'value' => 16
        ]);
    }
}
