<?php

use Illuminate\Database\Seeder;
use App\PaymentMethod;
class PaymentMethodsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PaymentMethod::create([
            'id' => 1,
            'name' => 'Pago en efectivo',
            'description' => 'Pago en efectivo'
        ]);
        PaymentMethod::create([
            'id' => 2,
            'name' => 'Transferencia Bancaria',
            'description' => 'Pago mediante transferencia bancaria por medio de referencia'
        ]);
    }
}
