<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->timestamps();
			$table->string('email', 50)->unique();
			$table->integer('id', true);
			$table->string('password', 191);
			$table->integer('persons_id')->nullable()->index('fk_persons_id');
			$table->string('remember_token', 100)->nullable();
			$table->string('username', 50)->unique();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
