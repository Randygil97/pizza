<?php
namespace App\Events;
use App\Order;
use App\User;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
class OrderStatusChanged implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    public $order;
    public $user;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($id,User $user)
    {
        $this->order = Order::with([
            'status_order',

            'orderPizzas',
            'orderPizzas.orders_pizza_toppings',
            'orderPizzas.orders_pizza_toppings.pizzas_topping',
            'delivery',
            'delivery.status_delivery',
            'payments',
            'payments.payment_method'

        ])->find($id);
        $this->user = $user;
    }
    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('users-orders.'.$this->user->id);
//        return ['private-pizza-tracker.'.$this->order->id, 'pizza-tracker'];
    }
    /**
     * Get the data to broadcast.
     *
     * @return array
     */
    public function broadcastWith()
    {
        $extra = [
//            'status_name' => $this->order->status->name,
//            'status_percent' => $this->order->status->percent,
        ];
        return array_merge($this->order->toArray(),$this->user->toArray(),$extra);
    }
}