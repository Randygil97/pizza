<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Payment extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'method_id' => $this->payment_methods_id,
            'method' => new PaymentMethod($this->whenLoaded('payment_method')),
            'reference' => $this->reference,
            'date_time' => $this->date_time

        ];
    }
}
