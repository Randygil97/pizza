<?php

namespace App\Http\Controllers\api;

use App\Http\Resources\Topping;
use App\Http\Resources\Toppings;
use App\PizzasTopping;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ToppingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $toppings = PizzasTopping::paginate($request->per_page);

        return  Topping::collection($toppings);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:50',
            'description' => 'required|string|max:300',
            'price' => 'required|integer',
        ]);
        $toppings = new PizzasTopping();
        $toppings->fill($request->all());
        $toppings->save();
        return ['success' => true];
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PizzasTopping $pizzasTopping
     * @return \Illuminate\Http\Response
     */
    public function show(PizzasTopping $pizzasTopping)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\PizzasTopping $pizzasTopping
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PizzasTopping $topping)
    {

        $this->validate($request, [
            'name' => 'required|string|max:50',
            'description' => 'required|string|max:300',
            'price' => 'required|integer',
        ]);
        $topping->fill($request->all());
        $topping->update();
        return ['success' => true,'topping' => new Topping($topping)];

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PizzasTopping $pizzasTopping
     * @return \Illuminate\Http\Response
     */
    public function destroy(PizzasTopping $topping)
    {
        $topping->delete();
        return ['success' => true];
    }
}