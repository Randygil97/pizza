<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::group(['middleware' => ['jwt.auth']], function() {
    Route::get('logout', 'AuthController@logout');
    Route::get('test', function(){
        return response()->json(['foo'=>'bar']);
    });
});
Route::get('test',function() {
    $order = App\Order::with([
        'status_order',
        'orderPizzas',
        'orderPizzas.orders_pizza_toppings',
        'orderPizzas.orders_pizza_toppings.pizzas_topping',
        'user',
        'user.person'
        ])->get();
    //$orderpizza = App\OrdersPizza::all();

    //return App\Http\Resources\OrderPizza::collection($orderpizza);
    //return new App\Http\Resources\Order(App\Order::find(1));
    return App\Http\Resources\Orders::collection($order);
});

Route::post('auth/login', 'AuthController@login');


Route::group(['middleware' => 'jwt.auth'], function(){
    Route::get('auth/user', 'AuthController@user');
    Route::resource('pizzas','api\PizzasController');
    Route::patch('pizzas/{pizza}/prices/{price}','api\PizzasController@updatePrice');
    Route::post('pizzas/{pizza}/prices/','api\PizzasController@storePrice');
    Route::delete('pizzas/prices/{price}','api\PizzasController@deletePrice');
    Route::get('roles','api\UsersController@getRoles');
    Route::resource('users','api\UsersController');
    Route::resource('orders','api\OrderController');
    Route::resource('toppings','api\ToppingsController');
    Route::resource('categories','api\PizzaCategoriesController');
    Route::get('statusOrder',function() {return App\Http\Resources\StatusOrder::collection(App\StatusOrder::all());});
    Route::get('deliveryman/{deliveryman}','api\DeliveryController@getDeliveryman');
    Route::post('deliveryman/{deliveryman}','api\DeliveryController@setDeliverymanStatus');
    Route::get('repartidores','api\OrderController@getRepartidores');
    Route::post('asignarRepartidor','api\OrderController@asignarRepartidor');
    Route::post('updateDeliveryStatus/{id}','api\OrderController@updateDeliveryStatus');
    Route::get('settings/IVA','SettingsController@getIVA');

    Route::get('reportByDay/{id}','DeliverymanController@reportByDay');

    Route::get('settings/dbBackup','SettingsController@dbBackup');

    Route::post('settings/IVA','SettingsController@setIVA');


});
Route::group(['middleware' => 'jwt.refresh'], function(){
    Route::get('auth/refresh', 'AuthController@refresh');
});
Route::group(['middleware' => 'jwt.auth'], function(){

    Route::post('auth/logout', 'AuthController@logout');
});