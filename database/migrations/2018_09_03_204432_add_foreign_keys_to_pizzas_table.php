<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToPizzasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('pizzas', function(Blueprint $table)
		{
			$table->foreign('pizzas_categories_id', 'fk_pizzas_categories_id')->references('id')->on('pizzas_categories')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('pizzas', function(Blueprint $table)
		{
			$table->dropForeign('fk_pizzas_categories_id');
		});
	}

}
