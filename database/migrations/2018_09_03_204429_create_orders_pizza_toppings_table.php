<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrdersPizzaToppingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('orders_pizza_toppings', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('orders_pizza_id')->nullable()->index('fk_orders_pizza_id_idx');
			$table->integer('pizza_toppings_id')->nullable()->index('fk_orders_pizza_toppings_idx');
			$table->integer('quantity')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('orders_pizza_toppings');
	}

}
