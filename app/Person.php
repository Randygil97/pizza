<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 10 Aug 2018 17:19:07 +0000.
 */

namespace App;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Person
 * 
 * @property int $id
 * @property string $name
 * @property string $lastname
 * @property string $address
 * @property string $gender
 * @property \Carbon\Carbon $born_date
 * @property string $document_type
 * @property string $document_number
 * 
 * @property \Illuminate\Database\Eloquent\Collection $phones
 * @property \Illuminate\Database\Eloquent\Collection $users
 *
 * @package App
 */
class Person extends Eloquent
{
	protected $table = 'persons';
	public $timestamps = false;



	protected $fillable = [
		'name',
		'lastname',
		'address',
		'gender',
		'born_date',
		'document_type',
		'document_number',
        'phone'
	];

	public function phones()
	{
		return $this->hasMany(\App\Phone::class, 'persons_id');
	}

	public function users()
	{
		return $this->hasMany(\App\User::class, 'persons_id');
	}
}
