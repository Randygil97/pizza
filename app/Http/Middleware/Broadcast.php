<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
class Broadcast
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $web = \Auth::guard('web')->user();
        if ($web) {
            return response()->json(\Illuminate\Support\Facades\Broadcast::auth($request));
        }

        return response()->json('Te quiero.', 500);
    }
}
