
<div id="loginModal" class="modal fade modal-danger" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            
            <div class="modal-header">
                <h3>Ingresar al sistema</h3>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <form class="form needs-validation" role="form" autocomplete="off" id="formLogin" novalidate="" method="POST">
                    <div class="form-group">
                        <a href="{{route('register')}}" class="float-right">¿No tienes cuenta?</a>
                        <label for="uname1">Usuario</label>
                        <input type="text" class="form-control form-control-lg" name="login" id="uname1" required>
                        <div class="invalid-feedback">Ingresa el usuario</div>
                    </div>
                    <div class="form-group">
                        <label>Password</label>
                        <input type="password" name="password" class="form-control form-control-lg" id="pwd1" required autocomplete="new-password">
                        <div class="invalid-feedback">Ingresa tu contraseña</div>
                    </div>
                    <div class="custom-control custom-checkbox">
                      <input type="checkbox" class="custom-control-input" name="rememberme" id="rememberMe">
                      <label class="custom-control-label" for="rememberMe">Recordar</label>
                    </div>
                    <div class="form-group py-4">
                        <button type="submit" class="btn btn-danger btn-lg float-right" id="btnLogin">Entrar</button>

                        <button class="btn btn-outline-secondary btn-lg" data-dismiss="modal" aria-hidden="true">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@push('scripts')
    
    <script>
      
        $("#formLogin").submit(function(e) {
            e.preventDefault();
             if (e.target.checkValidity() === false) {
                event.preventDefault();
                event.stopPropagation();
                return;
            }
            let post = $("#formLogin").serialize();
            let url = '{{ url('ajaxLogin') }}';
            
            let misCabeceras = new Headers();
            let csrf = $('meta[name="csrf-token"]').attr('content');
            
            let config = {
                        method: 'POST',
                        //body: JSON.stringify(data),
                        headers:{
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        mode: 'cors',
                        cache: 'default' 
                        };
            axios.post(url,post)
            .then(function (data) {
                data = data.data; 
                
               // window.location = data.redirect;
               window.location.reload();
            })
            .catch(function (error) {
                let errors = error.response.data.message;
                
                 for (error in  errors) { 
                    let msj = errors[error];
                    //console.log(msj,data.message);
                    toastr.error(msj);
                }
            });
            
        });
        $(".needs-validation").submit(function(e) {
            console.log(e.target.checkValidity());
            if (e.target.checkValidity() === false) {
                  event.preventDefault();
                    event.stopPropagation();
                }
            $(e.target).addClass('was-validated');
        });
          $(document).ready(function() {
            // Fetch all the forms we want to apply custom Bootstrap validation styles to
            var forms = document.getElementsByClassName('needs-validation');
            // Loop over them and prevent submission
            var validation = Array.prototype.filter.call(forms, function(form) {
            form.addEventListener('submit', function(event) {
                if (form.checkValidity() === false) {
                  event.preventDefault();
                    event.stopPropagation();
                }
                form.classList.add('was-validated');
            }, false);
            });
        });
    </script>
@endpush