<?php

use Illuminate\Database\Seeder;

class DeliverymenTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Deliveryman::create([
            'status' => 1,
            'users_id' => 4
        ]);
        App\Deliveryman::create([
            'status' => 1,
            'users_id' => 2
        ]);
    }
}
