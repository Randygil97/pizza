<?php

use Illuminate\Database\Seeder;
use App\StatusOrder;
class StatusOrderTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        StatusOrder::create([
               'id' => 1,
               'status' => 'Pendiente por cancelar/confirmar'
           ]);




        StatusOrder::create([
            'id' => 2,
           'status' => 'Pagada'
        ]);
        StatusOrder::create([
            'id' => 3,
            'status' => 'Preparación'
        ]);

        StatusOrder::create([
            'id' => 4,
            'status' => 'Terminada'
        ]);

        StatusOrder::create([
            'id' => 5,
            'status' => 'En ruta'
        ]);

        StatusOrder::create([
            'id' => 6,
            'status' => 'Entregada'
        ]);

        StatusOrder::create([
            'id' => 7,
            'status' => 'Cancelada'
        ]);

    }
}
