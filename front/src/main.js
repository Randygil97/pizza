import '@babel/polyfill'
import Vue from 'vue'
import './plugins/vuetify'
import App from './App.vue'
import router from './router'
import store from './store'
import './registerServiceWorker'
import axios from 'axios';
import VueAxios from 'vue-axios';
import VueRouter from 'vue-router';
import materialIcons from 'material-design-icons-iconfont';
import * as VueGoogleMaps from 'vue2-google-maps'
import VeeValidate from 'vee-validate';
import VueNoty from 'vuejs-noty'
import '../node_modules/nprogress/nprogress.css'

Vue.use(VueNoty, {
    timeout: 2000,
    progressBar: true,
    layout: 'topCenter'
})

const VueValidationEs = require('vee-validate/dist/locale/es');
window.axios = axios;

Vue.use(VeeValidate, {locale: 'es', dictionary: {es: VueValidationEs}});
Vue.use(VueGoogleMaps, {load: {key: '', libraries: 'places',}});
Vue.use(VueAxios, axios);
Vue.use(VueRouter);
Vue.use(materialIcons);
Vue.router = router;
Vue.config.productionTip = false

if (process.env.NODE_ENV!='production') {
  axios.defaults.baseURL = 'http://pizza.test/api';
} else {
  axios.defaults.baseURL = '/api';
}


Vue.use(require('@websanova/vue-auth'), {
    auth: require('@websanova/vue-auth/drivers/auth/bearer.js'),
    router: require('@websanova/vue-auth/drivers/router/vue-router.2.x.js'),
    rolesVar: 'role',
  http: require('@websanova/vue-auth/drivers/http/axios.1.x.js')
});
window.findOne = function (haystack, arr) {return arr.some(function (v) {return haystack.indexOf(v) >= 0;});};


window.vm = new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')

window.is = (role) => { 
  let userRole = vm.$auth.user().role;
  if (!Array.isArray(role)) {
    if (role==userRole) {
      return true;
    }
  }else {
    return role.find((e) => e.role==role) ? true : ''
  }
}