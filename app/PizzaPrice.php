<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 26 Aug 2018 01:28:39 +0000.
 */

namespace App;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class PizzaPrice
 * 
 * @property int $id
 * @property int $pizza_id
 * @property string $size
 * @property float $price
 * 
 * @property \App\Pizza $pizza
 *
 * @package App
 */
class PizzaPrice extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'pizza_id' => 'int',
		'price' => 'float'
	];

	protected $fillable = [
		'pizza_id',
		'size',
		'price'
	];

	public function pizza()
	{
		return $this->belongsTo(\App\Pizza::class);
	}
}
