<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use JWTAuth;
use Auth;
use App\User;
class AuthController extends Controller
{
    public function register(RegisterFormRequest $request)
    {
        $user = new User;
        $user->email = $request->email;
        $user->name = $request->name;
        $user->password = Hash::make($request->password);
        $user->save();
        return response([
            'status' => 'success',
            'data' => $user
        ], 200);
    }

    public function logout()
    {
        JWTAuth::invalidate();
        return response([
            'status' => 'success',
            'msg' => 'Logged out Successfully.'
        ], 200);
    }

    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');
        if ( ! $token = JWTAuth::attempt($credentials)) {
            return response([
                'status' => 'error',
                'error' => 'invalid.credentials',
                'msg' => 'Invalid Credentials.'
            ], 400);
        }
     
       $user = User::with('roles:id,name')->find(Auth::user()->id);
        $role = $user->roles[0]->name;
        
        return response([
            'status' => 'success',
            'role' => $role

        ])
            ->header('Authorization', $token);
    }

    public function user(Request $request)
    {
        //$user = User::find(Auth::user()->id)->get();
        //$user = Auth::user();
        //$user->push($user->getRoleNames()->implode('name'));
        //dd($user->getRoleNames()->implode('name'));
        //dd(Auth::user());

        //$user->push(['role' => $roles]);
//        $user = Auth::user();
//        $roles =  $user->getRoleNames()->implode('name',',');
//        $user->prueba = 'prueba';
//        $user->roles = $roles;
        //$uer->push(['roles'])
        $user = User::with('roles:id,name')->find(Auth::user()->id);


        $role = $user->roles[0]->name;
        $user->role = $role;
        return response([
            'status' => 'success',

            'data' =>  $user
          ]);
    }
    public function refresh()
    {
        return response([
            'status' => 'success'
        ]);
    }
}
