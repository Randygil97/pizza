<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePersonsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('persons', function(Blueprint $table)
		{
			$table->string('address', 200)->nullable();
			$table->date('born_date')->nullable();
			$table->string('document_number', 12)->nullable();
			$table->string('document_type', 1)->nullable();
			$table->string('gender', 1)->nullable();
			$table->integer('id', true);
			$table->string('lastname', 40)->nullable();
			$table->string('name', 40);
			$table->string('phone', 20)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('persons');
	}

}
