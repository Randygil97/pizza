<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Registro de entregas por día</title>
</head>
<body>
	<h1 style="text-align:center;">Entregas realizadas fecha {{ date('m-d-Y')}}</h1>
	
	<table class="table" style="width: 100%">
		<thead>
			<tr>
			<th>#</th>
			<th>Total</th>
			<th>Fecha del pedido</th>
			<th>Fecha de entrega</th>
			</tr>
		</thead>

		<tbody>

			@foreach($deliveries as $d) 

				<tr>
					<td>{{$d->order->id}}</td>
					<td>{{($d->order->iva/100*$d->order->total)+$d->order->total}} Bs.S</td>
					<td>{{$d->order->created_at->toDateTimeString()}}</td>
					<td>{{$d->delivered_time}}</td>

				</tr>
			@endforeach
		</tbody>
	</table>

</body>

<style>
	

	<style>
.table {
    font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

.table td, .table th {
    border: 1px solid #ddd;
    padding: 8px;
}

.table tr:nth-child(even){background-color: #f2f2f2;}

.table tr:hover {background-color: #ddd;}

.table th {
    padding-top: 12px;
    padding-bottom: 12px;
    text-align: left;
    background-color: #4CAF50;
    color: white;
}
</style>
</style>
</html>