<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UserPersonFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
                {
                    return [];
                }
            case 'POST':
                {
                    return [
                        'email' => 'required|unique:users|email|max:255',
                        'username'  => 'required|alpha_dash|max:20|unique:users',
                        'password'  => 'required|min:6|max:20',
                        'person.lastname'  => 'required|string|max:40',
                        'person.name'  => 'required|string|max:40',
                        "person.document_type" => 'required|max:1|string',
                        "person.document_number" => 'required|numeric|max:99999999|unique:persons,document_number',
                        'person.gender' => 'string|max:1',
                        'person.address' => 'string|max:500',
                        'person.phone' => 'numeric|max:99999999999',
                        'person.born_date' => 'date'
                    ];
                }
            case 'PUT':
            case 'PATCH':
                {
                    return [
                        'email' => 'required|email|max:255|unique:users,email,'.$this->id,
                        'username'  => 'required|alpha_dash|max:20|unique:users,username,'.$this->id,
                        'password'  => 'min:6|max:20',
                        'person.lastname'  => 'required|string|max:40',
                        'person.name'  => 'required|string|max:40',
                        "person.document_type" => 'required|max:1|string',
                        "person.document_number"  => [

                        'required',
                        'numeric',
                        'max:99999999',
                        Rule::unique('persons','document_number')->ignore($this->persons_id)
                        ],
                        'person.gender' => 'required|string|max:1',
                        'person.address' => 'required|string|max:500',
                        'person.phone' => 'required|numeric|max:99999999999',
                        'person.born_date' => 'required|date'
                    ];
                }
            default:break;
        }



    }
}
