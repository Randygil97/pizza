<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 03 Sep 2018 19:58:29 +0000.
 */

namespace App;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Deliveryman
 * 
 * @property int $id
 * @property int $users_id
 * @property int $status
 * 
 * @property \App\User $user
 * @property \Illuminate\Database\Eloquent\Collection $deliveries
 *
 * @package App
 */
class Deliveryman extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'users_id' => 'int',
		'status' => 'int'
	];

	protected $fillable = [
		'users_id',
		'status'
	];

	public function user()
	{
		return $this->belongsTo(\App\User::class, 'users_id');
	}

	public function deliveries()
	{
		return $this->hasMany(\App\Delivery::class, 'deliverymen_id');
	}
}
