<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToDeliveriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('deliveries', function(Blueprint $table)
		{
			$table->foreign('deliverymen_id', 'deliveries_ibfk_1')->references('id')->on('deliverymen')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('orders_id', 'fk_deliver_orders_id')->references('id')->on('orders')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('status_deliveries_id', 'fk_deliver_status_deliveries_id')->references('id')->on('status_deliveries')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('deliveries', function(Blueprint $table)
		{
			$table->dropForeign('deliveries_ibfk_1');
			$table->dropForeign('fk_deliver_orders_id');
			$table->dropForeign('fk_deliver_status_deliveries_id');
		});
	}

}
