<?php

namespace App\Http\Controllers;
use App\Payment;
use App\PizzasTopping;
use Illuminate\Http\Request;
use App\Order;
use Gloudemans\Shoppingcart\Facades\Cart;
use App\OrdersPizza;
use App\OrdersPizzaTopping;
use Auth;
use Illuminate\Support\Facades\DB;
class CheckoutController extends Controller
{
    function index() {
        $toppings = PizzasTopping::all()->keyBy('id');

        return view('frontend.checkout',compact('toppings'));
    }

    function orderReview(Request $request, $id) {
        $order = Order::with(['status_order','pizzas','payments'])->where('id',$id)->first();
        $pizzas = OrdersPizza::with('orders_pizza_toppings')->where('orders_id',$id)->get();
        /*dd($order[0]->pizzas[0]);*/
        return view('frontend.orderReview',compact('order','pizzas'));
    }

    public function orderFactura($id) {
        $order = Order::with(['status_order','pizzas','payments'])->where('id',$id)->first();

        $data = ["order"=>$order];

        $view = \View::make('pdf.OrderBill', compact('order'))->render();


        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view);
        $pdf->setpaper('a4', 'landscape');
        $gen =  $pdf->download('Randys-Factura-Fiscal-'.$id.'.pdf');
        return $gen;
        exit();


    }
    function makeOrder(Request $request) {

            $iva = \App\Options::where('option','iva')->first();

            return DB::transaction(function() use ($request,$iva) {

                if ($request->paymentMethod==1) {
                    $status = 2;
                }else {
                    $status = 1;
                }
                    $order = Order::create([
                        'users_id' => Auth::user()->id,
                        'status_orders_id' => $status,
                        'total' => Cart::subtotal(),
                        'deliver_lat' => $request->latLong["lat"],
                        'deliver_lng' => $request->latLong["lng"],
                        'deliver_address_info' => $request->address_info,

                        'iva' => $iva->value
                    ]);

                    foreach(Cart::content() as $item) {

                        $orderPizza = new OrdersPizza();
                        $orderPizza->pizzas_id = $item->id;
                        $orderPizza->orders_id = $order->id;
                        $orderPizza->special_preparation = $item->options->special_preparation;

                        $orderPizza->price = $item->price;
                        $orderPizza->save();

                        if (isset($item->options->toppingId)) {
                            for($i=0;$i<=count($item->options->toppingId) -1;$i++) {

                                $id = $item->options->toppingId[$i];
                                $qty = $item->options->toppingQty[$i];
                                OrdersPizzaTopping::create([
                                    'orders_pizza_id' => $orderPizza->id,
                                    'pizza_toppings_id' => $id,
                                    'quantity' => $qty
                                ]);
                            }
                        }
                    }

                $payment = new Payment();
                $payment->orders_id = $order->id;
                $payment->payment_methods_id = $request->paymentMethod;
                if ($request->paymentMethod==2) {
                    $payment->reference = $request->ref;
                }

                $payment->save();
                Cart::destroy();
                event(new \App\Events\OrderCreated($order->id));
                return ['success' => 'true','order' => $order,'payment' => $payment,'redirect' => url('/checkout/orderReview/'.$order->id)];
            });


            return ['success' => 'false'];


    }
}
