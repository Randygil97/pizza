<?php

namespace App\Http\Controllers;
use Cart;
use Illuminate\Http\Request;
use App\Pizza;
class CartController extends Controller
{
    public function add(Request $request) {
        $pizza = Pizza::findOrFail($request->id);

        parse_str($request->options, $options);

        $c = Cart::add($pizza->id,$pizza->name,$request->qty,$request->orderPrice,$options);
        Cart::setTax($c->rowId,16);
        return ["success" => true,"c" => $c];
    }
    public function getCart() {
        return Cart::content();
    }
    public function del(Request $request,$rowId) {
        $c = Cart::remove($rowId);
        return ["success" => true,"c" => $c];

    }
}
