<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 10 Aug 2018 17:19:07 +0000.
 */

namespace App;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class OrdersPizza
 * 
 * @property int $id
 * @property int $orders_id
 * @property int $pizzas_id
 * 
 * @property \App\Order $order
 * @property \App\Pizza $pizza
 * @property \Illuminate\Database\Eloquent\Collection $orders_pizza_toppings
 *
 * @package App
 */
class OrdersPizza extends Eloquent
{
	protected $table = 'orders_pizza';
	public $timestamps = false;

	protected $casts = [
		'orders_id' => 'int',
		'pizzas_id' => 'int',
        'price' => 'int'
	];

	protected $fillable = [
		'orders_id',
		'pizzas_id',
        'price',
        'special_preparation'
	];

	public function order()
	{
		return $this->belongsTo(\App\Order::class, 'orders_id');
	}

	public function pizza()
	{
		return $this->belongsTo(\App\Pizza::class, 'pizzas_id');
	}

	public function orders_pizza_toppings()
	{
		return $this->hasMany(\App\OrdersPizzaTopping::class);
	}
}
