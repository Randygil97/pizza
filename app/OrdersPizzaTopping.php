<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 10 Aug 2018 17:19:07 +0000.
 */

namespace App;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class OrdersPizzaTopping
 * 
 * @property int $id
 * @property int $orders_pizza_id
 * @property int $pizza_toppings_id
 * @property int $quantity
 * 
 * @property \App\PizzasTopping $pizzas_topping
 * @property \App\OrdersPizza $orders_pizza
 *
 * @package App
 */
class OrdersPizzaTopping extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'orders_pizza_id' => 'int',
		'pizza_toppings_id' => 'int',
		'quantity' => 'int'
	];

	protected $fillable = [
		'orders_pizza_id',
		'pizza_toppings_id',
		'quantity'
	];

	public function pizzas_topping()
	{
		return $this->belongsTo(\App\PizzasTopping::class, 'pizza_toppings_id');
	}

	public function orders_pizza()
	{
		return $this->belongsTo(\App\OrdersPizza::class);
	}
}
