<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Person extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
          'name' => $this->name,
          'lastname' => $this->lastname,
          'phone' => $this->phone,
          'address' => $this->address,
            'document_type' => $this->document_type,
            'document_number' => $this->document_number,
            'gender' => $this->gender,
            'born_date' => $this->born_date,

        ];
    }
}
