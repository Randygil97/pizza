<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrdersPizzaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('orders_pizza', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('orders_id')->nullable()->index('fk_orders_id_idx');
			$table->integer('pizzas_id')->nullable()->index('fk_orders_pizza_pizza_id_idx');
			$table->integer('price');
			$table->string('special_preparation', 300);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('orders_pizza');
	}

}
