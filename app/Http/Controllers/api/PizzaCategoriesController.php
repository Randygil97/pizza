<?php

namespace App\Http\Controllers\api;

use App\Http\Resources\PizzaCategory;
use App\PizzasCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PizzaCategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $per_page = $request->per_page ? $request->per_page : 5;

        $categories = PizzasCategory::paginate($per_page);

        return PizzaCategory::collection($categories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:50',
            'description' => 'required|string|max:300'
        ]);
        $categories = new PizzasCategory();
        $categories->fill($request->all());
        $categories->save();
        return ['success' => true];
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PizzasCategory $category
     * @return \Illuminate\Http\Response
     */
    public function show(PizzasCategory $category)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\PizzasCategory $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PizzasCategory $category)
    {

        $this->validate($request, [
            'name' => 'required|string|max:50',
            'description' => 'required|string|max:300'
        ]);
        $category->fill($request->all());
        $category->update();
        return ['success' => true,'category' => new PizzaCategory($category)];

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PizzasCategory $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(PizzasCategory $category)
    {
        $category->delete();
        return ['success' => true];
    }
}