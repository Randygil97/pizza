<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Deliveryman;
use DB;
class DeliverymanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function reportByDay(Request $request,$id) {
        



        $day = $request->day ? $request->day : 'now()';

        $deliveries = \App\Delivery::with('order')
        ->where(DB::RAW('DAY(delivered_time)'),DB::RAW("DAY(now())"))
        ->get();


        // ->whereHas('deliveries',function($query) use ($day) {
        //     $query->where([
        //         [DB::raw('DAY(created_at)'),DB::raw("DAY($day)")]
        //     ]); 
        // })  
        // ->get();


        $view = \View::make('pdf.DeliverymanDeliveriesByDay', compact('deliveries'))->render();


        //return $view;
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view);
        $pdf->setpaper('a4', 'landscape');
        $gen =  $pdf->download('Randys-Factura-Fiscal-'.$id.'.pdf');
        return $gen;
        exit();

    }
}
    
