<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Options;
use Spatie\DbDumper\Databases\MySql;
class SettingsController extends Controller
{
   
	public function dbBackup() {
		$file = public_path().'database.sql';

		MySql::create()
	    ->setDbName(env('DB_DATABASE'))
	    ->setUserName(env('DB_USERNAME'))
	    ->setPassword(env('DB_PASSWORD'))
	    ->dumpToFile($file);



		return response()->download($file);


	}

	public function getIVA() {
		$iva = Options::where('option','iva')->first();

		return response()->json(['iva' => $iva->value]);

	}

	public function setIVA(Request $request) {
		$iva = Options::where('option','iva')->first();
		$iva->value = $request->iva;
		$iva->update();
		return response()->json(['success' => true]);

	}
 }
