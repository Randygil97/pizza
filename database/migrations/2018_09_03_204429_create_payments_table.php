<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePaymentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('payments', function(Blueprint $table)
		{
			$table->dateTime('date_time')->nullable();
			$table->integer('id', true);
			$table->integer('orders_id')->index('fk_orders_id_idx');
			$table->integer('payment_methods_id')->index('fk_payment_methods_id_idx');
			$table->string('reference', 45)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('payments');
	}

}
