<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 10 Aug 2018 17:19:07 +0000.
 */

namespace App;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Payment
 * 
 * @property int $id
 * @property int $orders_id
 * @property int $payment_methods_id
 * @property string $reference
 * @property \Carbon\Carbon $date_time
 * 
 * @property \App\Order $order
 * @property \App\PaymentMethod $payment_method
 *
 * @package App
 */
class Payment extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'orders_id' => 'int',
		'payment_methods_id' => 'int'
	];

	protected $dates = [
		'date_time'
	];

	protected $fillable = [
		'orders_id',
		'payment_methods_id',
		'reference',
		'date_time'
	];

	public function order()
	{
		return $this->belongsTo(\App\Order::class, 'orders_id');
	}

	public function payment_method()
	{
		return $this->belongsTo(\App\PaymentMethod::class, 'payment_methods_id');
	}
}
