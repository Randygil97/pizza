
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
DROP TABLE IF EXISTS `deliveries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `deliveries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `deliverymen_id` int(11) NOT NULL,
  `delivered_time` timestamp NULL DEFAULT NULL,
  `orders_id` int(11) NOT NULL,
  `status_deliveries_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `deliverymen_id` (`deliverymen_id`),
  KEY `orders_id` (`orders_id`),
  KEY `status_deliveries_id` (`status_deliveries_id`),
  CONSTRAINT `deliveries_ibfk_1` FOREIGN KEY (`deliverymen_id`) REFERENCES `deliverymen` (`id`),
  CONSTRAINT `fk_deliver_orders_id` FOREIGN KEY (`orders_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_deliver_status_deliveries_id` FOREIGN KEY (`status_deliveries_id`) REFERENCES `status_deliveries` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `deliveries` WRITE;
/*!40000 ALTER TABLE `deliveries` DISABLE KEYS */;
INSERT INTO `deliveries` VALUES (1,1,'2018-09-10 03:34:10',2,3,'2018-09-09 03:33:22','2018-09-09 03:34:10'),(2,1,NULL,3,1,'2018-09-10 03:29:31','2018-09-10 03:29:31'),(3,1,NULL,1,1,'2018-09-10 03:46:02','2018-09-10 03:46:02'),(4,1,NULL,4,1,'2018-09-10 19:53:39','2018-09-10 19:53:39'),(5,1,NULL,5,1,'2018-09-10 19:55:11','2018-09-10 19:55:11'),(6,1,NULL,6,1,'2018-09-10 19:56:32','2018-09-10 19:56:32');
/*!40000 ALTER TABLE `deliveries` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `deliverymen`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `deliverymen` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` int(11) NOT NULL,
  `users_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `users_id` (`users_id`),
  CONSTRAINT `deliverymen_ibfk_1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `deliverymen` WRITE;
/*!40000 ALTER TABLE `deliverymen` DISABLE KEYS */;
INSERT INTO `deliverymen` VALUES (1,1,4);
/*!40000 ALTER TABLE `deliverymen` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2018_09_03_204429_create_deliveries_table',1),(2,'2018_09_03_204429_create_deliverymen_table',1),(3,'2018_09_03_204429_create_model_has_permissions_table',1),(4,'2018_09_03_204429_create_model_has_roles_table',1),(5,'2018_09_03_204429_create_orders_pizza_table',1),(6,'2018_09_03_204429_create_orders_pizza_toppings_table',1),(7,'2018_09_03_204429_create_orders_table',1),(8,'2018_09_03_204429_create_password_resets_table',1),(9,'2018_09_03_204429_create_payment_methods_table',1),(10,'2018_09_03_204429_create_payments_table',1),(11,'2018_09_03_204429_create_permissions_table',1),(12,'2018_09_03_204429_create_persons_table',1),(13,'2018_09_03_204429_create_phones_table',1),(14,'2018_09_03_204429_create_pizza_prices_table',1),(15,'2018_09_03_204429_create_pizzas_categories_table',1),(16,'2018_09_03_204429_create_pizzas_table',1),(17,'2018_09_03_204429_create_pizzas_toppings_table',1),(18,'2018_09_03_204429_create_role_has_permissions_table',1),(19,'2018_09_03_204429_create_roles_table',1),(20,'2018_09_03_204429_create_status_deliveries_table',1),(21,'2018_09_03_204429_create_status_orders_table',1),(22,'2018_09_03_204429_create_users_table',1),(23,'2018_09_03_204432_add_foreign_keys_to_deliveries_table',1),(24,'2018_09_03_204432_add_foreign_keys_to_deliverymen_table',1),(25,'2018_09_03_204432_add_foreign_keys_to_model_has_permissions_table',1),(26,'2018_09_03_204432_add_foreign_keys_to_model_has_roles_table',1),(27,'2018_09_03_204432_add_foreign_keys_to_orders_pizza_table',1),(28,'2018_09_03_204432_add_foreign_keys_to_orders_pizza_toppings_table',1),(29,'2018_09_03_204432_add_foreign_keys_to_orders_table',1),(30,'2018_09_03_204432_add_foreign_keys_to_payments_table',1),(31,'2018_09_03_204432_add_foreign_keys_to_phones_table',1),(32,'2018_09_03_204432_add_foreign_keys_to_pizzas_table',1),(33,'2018_09_03_204432_add_foreign_keys_to_role_has_permissions_table',1),(34,'2018_09_03_204432_add_foreign_keys_to_users_table',1),(35,'2018_09_10_140704_create_table_options',2);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `model_has_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `model_has_permissions` (
  `model_id` bigint(20) unsigned NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `permission_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`),
  CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `model_has_permissions` WRITE;
/*!40000 ALTER TABLE `model_has_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `model_has_permissions` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `model_has_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `model_has_roles` (
  `model_id` bigint(20) unsigned NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`),
  CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `model_has_roles` WRITE;
/*!40000 ALTER TABLE `model_has_roles` DISABLE KEYS */;
INSERT INTO `model_has_roles` VALUES (1,'App\\User',1),(3,'App\\User',4),(4,'App\\User',3),(5,'App\\User',4);
/*!40000 ALTER TABLE `model_has_roles` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `options`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `options` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `option` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `options` WRITE;
/*!40000 ALTER TABLE `options` DISABLE KEYS */;
INSERT INTO `options` VALUES (1,'iva','16');
/*!40000 ALTER TABLE `options` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders` (
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `date_time_order_delivered` datetime DEFAULT NULL,
  `deliver_address_info` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deliver_lat` double NOT NULL,
  `deliver_lng` double NOT NULL,
  `deliver_users_id` int(11) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `iva` int(11) NOT NULL,
  `status_orders_id` int(11) NOT NULL,
  `total` int(11) DEFAULT NULL,
  `users_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_delivers_users_id_idx` (`deliver_users_id`),
  KEY `fk_status_orders_id_idx` (`status_orders_id`),
  KEY `fk_deliver_users_id_idx` (`users_id`),
  CONSTRAINT `fk_deliver_users_id_idx` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_delivers_users_id_idx` FOREIGN KEY (`deliver_users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_status_orders_id_idx` FOREIGN KEY (`status_orders_id`) REFERENCES `status_orders` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
INSERT INTO `orders` VALUES ('2018-09-09 02:14:42','2018-09-10 03:46:02',NULL,'dfgsdfsd',9.90500981826926,-67.35678949737547,NULL,1,16,5,23,1),('2018-09-09 03:31:47','2018-09-09 03:34:11',NULL,'casa numerpo 12',9.901712300180161,-67.35949316406248,NULL,2,16,6,58,1),('2018-09-09 06:36:01','2018-09-10 03:29:31',NULL,'casa numero 20',19.413714872474575,-100.123541806221,NULL,3,16,5,12,1),('2018-09-10 19:52:44','2018-09-10 19:53:39',NULL,'fsgfd',9.908814605654566,-67.35138216400145,NULL,4,16,5,8,1),('2018-09-10 19:54:25','2018-09-10 19:55:11',NULL,'dfgsdf',9.90932190730492,-67.35219755554198,NULL,5,16,5,12,1),('2018-09-10 19:55:46','2018-09-10 19:56:32',NULL,'were',9.910082858309156,-67.35399999999998,NULL,6,16,5,8,1),('2018-09-10 19:57:14','2018-09-10 19:57:42',NULL,'reterfg',9.909913758238568,-67.3530129470825,NULL,7,16,4,8,1);
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `orders_pizza`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders_pizza` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `orders_id` int(11) DEFAULT NULL,
  `pizzas_id` int(11) DEFAULT NULL,
  `price` int(11) NOT NULL,
  `special_preparation` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_orders_id_idx` (`orders_id`),
  KEY `fk_orders_pizza_pizza_id_idx` (`pizzas_id`),
  CONSTRAINT `fk_orders_pizza_orders_id` FOREIGN KEY (`orders_id`) REFERENCES `orders` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_orders_pizza_pizza_id` FOREIGN KEY (`pizzas_id`) REFERENCES `pizzas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `orders_pizza` WRITE;
/*!40000 ALTER TABLE `orders_pizza` DISABLE KEYS */;
INSERT INTO `orders_pizza` VALUES (1,1,1,23,'dfsdf'),(2,2,1,39,'bordes tostados'),(3,2,3,19,''),(4,3,1,12,''),(5,4,1,8,''),(6,5,1,12,''),(7,6,1,8,''),(8,7,1,8,'');
/*!40000 ALTER TABLE `orders_pizza` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `orders_pizza_toppings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders_pizza_toppings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `orders_pizza_id` int(11) DEFAULT NULL,
  `pizza_toppings_id` int(11) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_orders_pizza_id_idx` (`orders_pizza_id`),
  KEY `fk_orders_pizza_toppings_idx` (`pizza_toppings_id`),
  CONSTRAINT `fk_orders_pizza_toppings` FOREIGN KEY (`pizza_toppings_id`) REFERENCES `pizzas_toppings` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_toppings_orders_pizza_id` FOREIGN KEY (`orders_pizza_id`) REFERENCES `orders_pizza` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `orders_pizza_toppings` WRITE;
/*!40000 ALTER TABLE `orders_pizza_toppings` DISABLE KEYS */;
/*!40000 ALTER TABLE `orders_pizza_toppings` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `created_at` datetime DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `payment_methods`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payment_methods` (
  `description` varchar(90) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `payment_methods` WRITE;
/*!40000 ALTER TABLE `payment_methods` DISABLE KEYS */;
INSERT INTO `payment_methods` VALUES ('Pago en efectivo',1,'Pago en efectivo'),('Pago mediante transferencia bancaria por medio de referencia',2,'Transferencia Bancaria');
/*!40000 ALTER TABLE `payment_methods` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `payments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payments` (
  `date_time` datetime DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `orders_id` int(11) NOT NULL,
  `payment_methods_id` int(11) NOT NULL,
  `reference` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_orders_id_idx` (`orders_id`),
  KEY `fk_payment_methods_id_idx` (`payment_methods_id`),
  CONSTRAINT `fk_orders_id` FOREIGN KEY (`orders_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_payment_methods_id` FOREIGN KEY (`payment_methods_id`) REFERENCES `payment_methods` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `payments` WRITE;
/*!40000 ALTER TABLE `payments` DISABLE KEYS */;
INSERT INTO `payments` VALUES (NULL,1,1,2,'123456'),(NULL,2,2,2,'123567'),(NULL,3,3,2,'12356798'),(NULL,4,4,2,'12354'),(NULL,5,5,2,'123456'),(NULL,6,6,2,'123456'),(NULL,7,7,2,'1234576');
/*!40000 ALTER TABLE `payments` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissions` (
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `persons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `persons` (
  `address` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `born_date` date DEFAULT NULL,
  `document_number` varchar(12) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `document_type` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lastname` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `persons` WRITE;
/*!40000 ALTER TABLE `persons` DISABLE KEYS */;
INSERT INTO `persons` VALUES ('Por ahi','1997-05-20','26428919','V','M',1,'Gil','Randy','04143461587'),('Por ahi','1997-05-20','2542345','V','M',2,'Perez','Pedro','04143461587'),('Avenida Alonzo número 3878, Encino, California','1997-05-20','12412234','V','M',3,'Gutierritos','Jefferson','04125487945'),('Avenida Alonzo número 3878, Encino, California','1997-05-20','12412234','V','M',4,'Yakitori','Alvin','04168457845'),(NULL,NULL,'123567','V',NULL,5,'emplead','empleado',NULL);
/*!40000 ALTER TABLE `persons` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `phones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `number` varchar(12) COLLATE utf8mb4_unicode_ci NOT NULL,
  `persons_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_persons_id_idx` (`persons_id`),
  CONSTRAINT `fk_phone_persons_id` FOREIGN KEY (`persons_id`) REFERENCES `persons` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `phones` WRITE;
/*!40000 ALTER TABLE `phones` DISABLE KEYS */;
/*!40000 ALTER TABLE `phones` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `pizza_prices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pizza_prices` (
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pizza_id` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `size` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `pizza_prices` WRITE;
/*!40000 ALTER TABLE `pizza_prices` DISABLE KEYS */;
INSERT INTO `pizza_prices` VALUES (NULL,NULL,1,1,8,'Pequeña'),(NULL,NULL,2,1,12,'Mediana'),(NULL,NULL,3,1,15,'Grande'),(NULL,NULL,4,2,8,'Pequeña'),(NULL,NULL,5,2,120,'Mediana'),(NULL,NULL,6,2,15,'Grande'),(NULL,NULL,7,3,12,'Pequeña'),(NULL,NULL,8,3,15,'Mediana'),(NULL,NULL,9,3,19,'Grande');
/*!40000 ALTER TABLE `pizza_prices` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `pizzas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pizzas` (
  `description` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pizzas_categories_id` int(11) DEFAULT NULL,
  `sale_price` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_pizzas_categories_id_idx` (`pizzas_categories_id`),
  CONSTRAINT `fk_pizzas_categories_id` FOREIGN KEY (`pizzas_categories_id`) REFERENCES `pizzas_categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `pizzas` WRITE;
/*!40000 ALTER TABLE `pizzas` DISABLE KEYS */;
INSERT INTO `pizzas` VALUES ('Peperoni, Queso, Olivas negras, Salsa de Tomate, Hongos, Carne, Cebolla, Ensalada',1,'pizza-1.jpg','Pizza Margarita',1,NULL),('Tomate, oregano, ajo, aceite de oliva extra virgen, peperoni, queso, cebolla roja, salsa y hongos.',2,'pizza-6.jpg','Pizza Napolitana',1,NULL),('Tomate frito, Calabacín, Berenjena,Queso Mozzarella, Queso rallado',3,'pizza-4.jpg','Berenjena y Calabacin',2,NULL);
/*!40000 ALTER TABLE `pizzas` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `pizzas_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pizzas_categories` (
  `description` varchar(90) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `pizzas_categories` WRITE;
/*!40000 ALTER TABLE `pizzas_categories` DISABLE KEYS */;
INSERT INTO `pizzas_categories` VALUES ('Pizzas Básicas',1,'Básicas'),('Pizzas Vegetarianas',2,'Vegetarianas');
/*!40000 ALTER TABLE `pizzas_categories` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `pizzas_toppings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pizzas_toppings` (
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `pizzas_toppings` WRITE;
/*!40000 ALTER TABLE `pizzas_toppings` DISABLE KEYS */;
INSERT INTO `pizzas_toppings` VALUES ('50g de Champiñones picados',1,'Champiñones',3),('30g de Champiñones picados',2,'Salchichón',5),('20g de Champiñones picados',3,'Maíz',5),('30g de Champiñones picados',4,'Tocineta',7),('20g de Champiñones picados',5,'Maíz',5);
/*!40000 ALTER TABLE `pizzas_toppings` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `role_has_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_has_permissions` (
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `role_has_permissions_role_id_foreign` (`role_id`),
  CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `role_has_permissions` WRITE;
/*!40000 ALTER TABLE `role_has_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_has_permissions` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES ('2018-09-08 23:53:51','2018-09-08 23:53:51','web',1,'admin'),('2018-09-08 23:53:51','2018-09-08 23:53:51','web',2,'cliente'),('2018-09-08 23:53:51','2018-09-08 23:53:51','web',3,'repartidor'),('2018-09-08 23:53:51','2018-09-08 23:53:51','web',4,'empleado');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `status_deliveries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `status_deliveries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `status_deliveries` WRITE;
/*!40000 ALTER TABLE `status_deliveries` DISABLE KEYS */;
INSERT INTO `status_deliveries` VALUES (1,'Lista para enviar'),(2,'En ruta'),(3,'Entregada');
/*!40000 ALTER TABLE `status_deliveries` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `status_orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `status_orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `status_orders` WRITE;
/*!40000 ALTER TABLE `status_orders` DISABLE KEYS */;
INSERT INTO `status_orders` VALUES (1,'Pendiente por cancelar/confirmar'),(2,'Pagada'),(3,'Preparación'),(4,'Terminada'),(5,'En ruta'),(6,'Entregada'),(7,'Cancelada');
/*!40000 ALTER TABLE `status_orders` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `email` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `persons_id` int(11) DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  UNIQUE KEY `users_username_unique` (`username`),
  KEY `fk_persons_id` (`persons_id`),
  CONSTRAINT `fk_persons_id` FOREIGN KEY (`persons_id`) REFERENCES `persons` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES ('2018-09-08 23:53:52','2018-09-08 23:53:52','r.gilcamejo@gmail.com',1,'$2y$10$a3gsuMB0wEYF3z1e9/5Swuk/4B1DsTS4b81tY27SRB0blBo3QXR32',1,'7BANO7qeRBJUBZuqXtOCJb0zmnXqrNcKT3q3Jftz7pMaiG2aWJDYIUtDFpE5','randy'),('2018-09-08 23:53:53','2018-09-08 23:53:53','jefferson@gmail.com',3,'$2y$10$pnl.RSDj0PLbSFn4fRh/Ue1Vd/Xzwy2k69RIAvIrn/EvR.QcE8GZO',3,NULL,'jefferson'),('2018-09-08 23:53:53','2018-09-08 23:53:53','alvin@gmail.com',4,'$2y$10$YuWexjCSAaGtiQ2U32abtO6j6pfXprt5x9nDZwAJcmD0mYpTYJKMW',4,NULL,'alvin'),('2018-09-10 19:13:42','2018-09-10 19:13:42','empleado@gmail.com',5,'123456',5,NULL,'empleado');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

