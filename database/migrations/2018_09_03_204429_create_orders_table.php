<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrdersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('orders', function(Blueprint $table)
		{
			$table->timestamps();
			$table->dateTime('date_time_order_delivered')->nullable();
			$table->string('deliver_address_info', 500)->nullable();
			$table->float('deliver_lat', 10, 0);
			$table->float('deliver_lng', 10, 0);
			$table->integer('deliver_users_id')->nullable()->index('fk_delivers_users_id_idx');
			$table->integer('id', true);
			$table->integer('iva');
			$table->integer('status_orders_id')->index('fk_status_orders_id_idx');
			$table->integer('total')->nullable();
			$table->integer('users_id')->index('fk_deliver_users_id_idx');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('orders');
	}

}
