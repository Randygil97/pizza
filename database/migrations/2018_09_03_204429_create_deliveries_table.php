<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDeliveriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('deliveries', function(Blueprint $table)
		{

			$table->integer('id', true);
            $table->integer('deliverymen_id')->index('deliverymen_id');
            $table->timestamp('delivered_time')->nullable();
            $table->integer('orders_id')->index('orders_id');
			$table->integer('status_deliveries_id')->index('status_deliveries_id');
			$table->timestamps();
//			$table->integer('users_id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('deliveries');
	}

}
