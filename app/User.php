<?php
namespace App;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable  implements JWTSubject   {
    use Notifiable;
    use HasRoles;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username',
        'email',
        'password',
        'persons_id'
    ];

    // Rest omitted for brevity

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
    public function person() {
        return $this->belongsTo(\App\Person::class, 'persons_id');
    }

    public function orders() {
        return $this->hasMany(\App\Order::class, 'users_id');
    }

    //return $this->belongsToMany(\App\Pizza::class, 'orders_pizza', 'orders_id', 'pizzas_id')
    //->withPivot('id');
//
//    public function deliveries() {
//        return $this->belongsToMany(\App\Delivery::class, 'deliverymen', 'deliverymen_id', 'pizzas_id')
//            ->withPivot('id');
//    }
    public function deliverymen()
    {
        return $this->hasMany(\App\Deliveryman::class, 'users_id');
    }
    public function deliveryman()
    {
        return $this->hasOne(\App\Deliveryman::class, 'users_id');
    }


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}