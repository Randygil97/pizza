<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Delivery extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return  [
            'id' => $this->id,
            'status' => new DeliveryStatus($this->whenLoaded('status_delivery')),
            'deliveryman' => new Deliveryman($this->whenLoaded('deliveryman'))
        ];
    }
}
