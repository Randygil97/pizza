<?php

namespace App\Http\Controllers\auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\User;
class AjaxLoginController extends Controller
{
    public function login(Request $request)  {  

        $field = filter_var($request->input('login'), FILTER_VALIDATE_EMAIL) ? 'email' : 'username';
        $request->merge([$field => $request->input('login')]);
        $response = [];
        $validator = \Validator::make($request->all() , [
            'login' => 'required|max:30',
            'password' => 'required|min:6',
        ]);

        if (!$validator->passes()) {
            $errors = $validator->errors();
            $errors =  json_decode($errors); 
            return response()->json([
                'success' => false,
                'message' => $errors
            ], 422);
        }


        if (Auth::attempt($request->only($field, 'password'))) {
            return response()->json([
                'success' => true,
                'redirect' => url('/')
            ], 200);
        }else {
            return response()->json([
                'success' => false,
                'message' => [
                    'error' => 'Usuario o contraseña incorrecta.'
                ]
            ], 422);
        }
      /*
        if($validator->fails()) { 
              return response()->json(['success' => false, 'errors' => $validator->errors()->all()]);
          } else {
              // create our user data for the authentication
              $userdata = array(
                  'email'     => Input::get('email'),
                  'password'  => Input::get('password')
              );
              // attempt to do the login
              if (Auth::attempt($userdata)) {
                  return response()->json(['success' => true, 'redirectto' => 'dashboard']);
              } else {
                  return response()->json(['success' => false, 'error' => ['Login Failed! Username and password is incorrect.']]);
              }
          }
          */
      }
}
