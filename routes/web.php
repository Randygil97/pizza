<?php

/* Production route */
Route::get('/admin/{any?}', 'SpaController@index')->where('any', '.*');
Route::get('/pizzasGet',function() {
    return App\Pizza::all();
});
Route::get('tet',function() {
    return ;
});

Route::resource('pizzas','PizzaController');
Route::get('orders','OrderController@index');


/* Rutas checkout */
Route::get('checkout','CheckoutController@index');
Route::post('/checkout/makeOrder','CheckoutController@makeOrder');
Route::get('/checkout/orderReview/{id}','CheckoutController@orderReview');
Route::get('/checkout/orderFactura/{id}','CheckoutController@orderFactura');
Route::post('/ajaxLogin','Auth\AjaxLoginController@login');
Route::get('reportByDay/{id}','DeliverymanController@reportByDay');


Route::get('/', function () {
    return view('frontend.index');
})->name('index');



/* Cart Routes */
Route::post('/cart/add/','CartController@add');
Route::post('/cart/del/{rowId}','CartController@del');

Route::get('/cart/','CartController@getCart');

Auth::routes();
Route::post('guard/broadcast/auth', function(\Illuminate\Support\Facades\Request $req){
    return true;
})->middleware('broadcast');



Route::get('login',function() {
	return redirect('/');
})->name('login');


Route::get('/home', 'HomeController@index')->name('home');
