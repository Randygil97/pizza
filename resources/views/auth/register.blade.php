@extends('layouts.frontend.app')

@section('content')
<div class="container-wrapper">
    <div class="page-title-wrapper">
        <div class="center">
            <div class="page-title">
                <div class="page-title-preimage"><img alt="Pretitle-image" src="http://pizza.test/frontend/images/pizza.svg"></div>

                <h1>Registro</h1>
                <div class="clear"></div>
            </div>
        </div>
    </div>
    <div class="clear"></div>
        <div id="container">
            <!-- start container -->
            <div class="center">
                <div class="page-wrapper">
                    <div class="main">               
                        <div class="row justify-content-center">
                            <div class="col-md-8">
                                <div class="card card-primary">
                                    <div class="card-header bg-danger text-white">Registrar</div>
                                    <div class="card-body">
                                        <form method="POST" action="{{ route('register') }}" aria-label="{{ __('Register') }}">
                                            @csrf
                                            <div class="row">
                                                <div class=" col-sm-2"> 
                                                     <label for="tipo">Tipo</label>

                                                    <select class="form-control" id="tipo" name="document_type">
                                                        <option value="V">V</option>
                                                        <option value="E">E</option>
                                                    </select>
                                                </div>
                                                <div class=" col-sm-4">
                                                    <label for="email">Cédula</label>

                                                    <input id="document_number" type="text" placeholder="Cédula" class="form-control{{ $errors->has('document_number') ? ' is-invalid' : '' }}" name="document_number" value="{{ old('document_number') }}" required autofocus>

                                                    @if ($errors->has('document_number'))
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('document_number') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>

                                                <div class="form-group col-sm-6 "> 
                                                    <div class="">
                                                        <label for="name">Género</label>

                                                         
                                                        <select name="gender" id="" class="form-control">
                                                            <option value="M">Masculino</option>
                                                            <option value="F">Femenino</option>
                                                            
                                                        </select>
                                                        @if ($errors->has('password'))
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $errors->first('password') }}</strong>
                                                            </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                
                                            </div>

                                            <div class="row">
                                                
                                                <div class="form-group  col-sm-6">

                                                    <div class="">
                                                         <label for="username">Nombre de Usuario</label>

                                                        <input id="text" type="text" placeholder="Nombre de Usuario" class="form-control{{ $errors->has('username') ? ' is-invalid' : '' }}" name="username" value="{{ old('username') }}" required>

                                                        @if ($errors->has('username'))
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $errors->first('username') }}</strong>
                                                            </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="form-group  col-sm-6">

                                                    <div class="">
                                                         <label for="email">Correo Electrónico</label>

                                                        <input id="email" type="email" placeholder="Correo Electrónico" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                                        @if ($errors->has('email'))
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $errors->first('email') }}</strong>
                                                            </span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="form-group col-sm-6 "> 
                                                    <div class="">
                                                        <label for="name">Contraseña</label>

                                                         <input id="password" type="password"  placeholder="Contraseña" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                                        @if ($errors->has('password'))
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $errors->first('password') }}</strong>
                                                            </span>
                                                        @endif
                                                     </div>
                                                </div>

                                                <div class="form-group  col-sm-6">

                                                    <div class="">
                                                        <label for="email">Confirmar Contraseña</label>

                                                        <input id="password_confirmation" type="password" placeholder="Confirmar Contraseña" class="form-control{{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}" name="password_confirmation" required>

                                                       
                                                        @if ($errors->has('password_confirmation'))
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $errors->first('password_confirmation') }}</strong>
                                                            </span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="form-group col-sm-6 "> 
                                                    <div class="">
                                                        <label for="name">Nombres</label>

                                                        <input id="name" type="text" placeholder="Nombres" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>

                                                        @if ($errors->has('name'))
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $errors->first('name') }}</strong>
                                                            </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                 <div class="form-group col-sm-6 "> 
                                                    <div class="">
                                                        <label for="name">Apellidos</label>

                                                        <input id="lastname" placeholder="Apellidos" type="text" class="form-control{{ $errors->has('lastname') ? ' is-invalid' : '' }}" name="lastname" value="{{ old('lastname') }}" required autofocus>

                                                        @if ($errors->has('lastname'))
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $errors->first('lastname') }}</strong>
                                                            </span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                            

                                         

                                            <div class="row">
                                                <div class="form-group col-sm-6 "> 
                                                    <div class="">
                                                        <label for="name">Telefono</label>

                                                        <input id="phone" placeholder="Teléfono" type="number" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone" value="{{ old('phone') }}" required autofocus>

                                                        @if ($errors->has('phone'))
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $errors->first('phone') }}</strong>
                                                            </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                 <div class="form-group col-sm-6 "> 
                                                    <div class="">
                                                        <label for="name">Dirección</label>

                                                        <textarea name="address" placeholder="Dirección" class="form-control{{ $errors->has('address') ? ' is-invalid' : '' }}" placeholder="Dirección" >{{ old('address') }}</textarea>

                                                        @if ($errors->has('lastname'))
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $errors->first('lastname') }}</strong>
                                                            </span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                            
                                               <div class="row">
                                                <div class="form-group col-sm-6 "> 
                                                    <div class="">
                                                        <label for="name">Fecha de Nacimiento</label>

                                                        <input id="born_date" type="date" class="form-control{{ $errors->has('born_date') ? ' is-invalid' : '' }}" name="born_date" value="{{ old('born_date') }}" required autofocus>

                                                        @if ($errors->has('born_date'))
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $errors->first('born_date') }}</strong>
                                                            </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                
                                            </div>
                                            
                                            <div class="form-group row mb-0 justify-content-center  ">
                                                <div class="">
                                                    <button type="submit" class="btn btn-danger">
                                                        Registrar
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>        
                            </div>
                        </div>
                    </div>
                </div>
            </div>
         </div>
      </div>
  

  <style>
  </style>
@endsection

