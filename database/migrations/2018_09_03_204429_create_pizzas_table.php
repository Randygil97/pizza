<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePizzasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pizzas', function(Blueprint $table)
		{
			$table->string('description', 200)->nullable();
			$table->integer('id', true);
			$table->string('image', 45)->nullable();
			$table->string('name', 45)->nullable();
			$table->integer('pizzas_categories_id')->nullable()->index('fk_pizzas_categories_id_idx');
			$table->integer('sale_price')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pizzas');
	}

}
