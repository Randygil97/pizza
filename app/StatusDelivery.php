<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 01 Sep 2018 15:57:13 +0000.
 */

namespace App;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class StatusDelivery
 * 
 * @property int $id
 * @property int $name
 *
 * @package App
 */
class StatusDelivery extends Eloquent
{
	public $timestamps = false;


	protected $fillable = [
	    'id',
		'name'
	];
}
