<?php

namespace App\Http\Controllers\api;

use App\Http\Requests\UserPersonFormRequest;
use App\Person;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $per_page = $request->per_page ? $request->per_page : 5;
        $users = User::with(['person','roles'])->where('id','!=',\Auth::user()->id)->paginate($per_page);

        return \App\Http\Resources\User::collection($users);


    }


    public function getRoles() {

        $roles = \Spatie\Permission\Models\Role::all();

        return compact('roles');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserPersonFormRequest $request)
    {


        return \DB::transaction(function() use ($request) {

            $person = new Person();

            $person->fill([
                'name' => $request->person["name"],
                'lastname' => $request->person["lastname"],
                'document_type' => $request->person["document_type"],
                'document_number' => $request->person["document_number"]


            ]);
            $user = new User();
            $user->fill($request->except('person'));
            $person->save();
            $user->persons_id = $person->id;
//            return ['role' => Role::where('id',$request->role)->first(),'rol' => $request->role];

            $user->save();


            $user->syncRoles($request->role);
            $user = new \App\Http\Resources\User($user);
            return ['success' => true,'user' => $user];
        });
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(UserPersonFormRequest $request, User $user)
    {

//        return [$request->all();

        $user->fill($request->except(['person','password']));
        $user->password = Hash::make($request->password);
        $person = $user->person;
        $person->fill([
            'name' => $request->person["name"],
            'lastname' => $request->person["lastname"],
            'document_type' => $request->person["document_type"],
            'document_number' => $request->person["document_number"]

        ]);
        $user->update();
        $person->update();

        return ['success' => true, 'user' => $user];

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->delete();

        return ['success' => true];
    }
}
