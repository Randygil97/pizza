<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Person;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/pizzas';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'lastname' => 'required|string|max:255',
            'username' => 'required|string|max:50|unique:users',
            "document_type" => 'required|max:1|string',
            "document_number" => 'required|numeric|max:99999999|unique:persons',
            'gender' => 'required|string|max:1',
            'address' => 'required|string|max:500',
            'phone' => 'required|numeric|max:99999999999',
            'born_date' => 'required|date',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);

    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $person = '';
        $user = '';
        return \DB::transaction(function() use ($data) {
            $person = Person::create([
                "document_type" => $data['document_type'],
                "document_number" => $data['document_number'],
                "name" => $data['name'],
                "gender" => $data['gender'],
                "lastname" => $data['lastname'],
                "phone" => $data['phone'],
                "address" => $data['address'],
                "born_date" => $data['born_date']
            ]);
            $user = User::create([
                'name' => $data['name'],
                "username" => $data['username'],
                'email' => $data['email'],
                'password' => Hash::make($data['password']),
                'persons_id' => $person->id
            ]);
            $user->assignRole('cliente');
            return $user;
        }); 
    }
}
