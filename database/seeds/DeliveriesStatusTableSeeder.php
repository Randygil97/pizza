<?php

use Illuminate\Database\Seeder;
use App\StatusDelivery;
class DeliveriesStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        StatusDelivery::create([
           'id' => 1,
           'name' => 'Lista para enviar'
        ]);
        StatusDelivery::create([
            'id' => 2,
            'name' => 'En ruta'
        ]);
        StatusDelivery::create([
            'id' => 3,
            'name' => 'Entregada'
        ]);
    }
}
