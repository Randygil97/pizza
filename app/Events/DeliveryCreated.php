<?php

namespace App\Events;

use App\Order;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class DeliveryCreated implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    protected $order,$userid;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($id)
    {

        $this->order = Order::with([
            'status_order',
            'orderPizzas',
            'orderPizzas.orders_pizza_toppings',
            'orderPizzas.orders_pizza_toppings.pizzas_topping',
            'delivery',
            'delivery.deliveryman',
            'delivery.status_delivery',
            'payments',
            'payments.payment_method'

        ])->find($id);
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('orders');
    }

    public function broadcastWith()
    {
        $extra = [
//            'status_name' => $this->order->status->name,
//            'status_percent' => $this->order->status->percent,
        ];
        return array_merge($this->order->toArray(),$extra);
    }
}
