<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Orders extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            'id' => $this->id,
            'iva' => $this->iva,
            'subtotal' => $this->total,
            'total' => $this->totalorder,
            'deliver_address_info' => $this->deliver_address_info,
            'deliver_lat' => $this->deliver_lat,
            'deliver_lng' => $this->deliver_lng,
            'delivery' => new Delivery($this->whenLoaded('delivery')),
            'user' => new User($this->whenLoaded('user')),
            'status' => new StatusOrder($this->whenLoaded('status_order')),
            'payment' => new Payment($this->whenLoaded('payments')),
            'pizzas' => OrderPizza::collection($this->whenLoaded('orderPizzas'))



        ];
    }
}


