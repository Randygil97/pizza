@extends('layouts.frontend.app')

@section('content')
    <link rel="stylesheet" href="{{asset('frontend/css/payment.css')}}" xmlns="http://www.w3.org/1999/html">
    <div class="container-wrapper">
        <div class="page-title-wrapper">
            <div class="center">
                <div class="page-title">
                    <div class="page-title-preimage"><img alt="Pretitle-image" src="{{asset('frontend/images/pizza.svg') }}"></div>
                    <h1>Orden</h1>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
        <div class="clear"></div>
        <div id="container">
            <!-- start container -->
            <div class="center">
                <div class="page-wrapper">
                    <div class="main" >
                        <div id="app">
                           <main class="page payment-page">
                            <h1>Ordenes </h1>

                            <table class="table table-striped table-hover">
                            <thead>
                                <th>N#</th>
                                <th>Monto</th>
                                <th>Status</th>
                                <th>Acciones</th>
                            </thead>

                            <tbody>
                            <template v-for="(row, rindex) in orders">
                                <tr >
                                    <td>@{{row.id}}</td>
                                    <td>@{{row.iva}}</td>
                                    <td>@{{row.status_order.status}} <span v-if="row.delivery">(@{{ row.delivery.status_delivery.name}})</span></td>
                                    <td><button class="btn btn-primary"><i class="fa fa-download"></i> Descargar Factura</button></td>
                                </tr>
                            </template>

                            {{--@foreach($orders as $order)--}}
                                {{--<tr>--}}
                                    {{--<td>{{$order->id}}</td>--}}
                                    {{--<td>{{$order->iva/100+$order->total+$order->total}}</td>--}}
                                    {{--<td>{{$order->status}}--}}
                                        {{--@if(isset($order->delivery->status_delivery->id))--}}
                                            {{--({{$order->delivery->status_delivery->name}})--}}
                                        {{--@endif--}}
                                    {{--</td>--}}
                                    {{--<td><a href="{{url('checkout/orderFactura/'.$order->id)}}" class="btn btn-primary"><i class="fa fa-download"></i> Descargar Factura Digital</a></td>--}}
                                {{--</tr>--}}
                            {{--@endforeach--}}

                            </tbody>
                            </table>
                           </main>
                         </div>
                    </div>

                    </main>

                </div>
                <div class="clear"></div>
            </div>
        </div>
    </div>
    <!-- end container -->
    <div class="clear"></div>
    </div>
    <!-- end container-wrapper -->


    @push('scripts')
        <script src="https://cdn.jsdelivr.net/npm/vue@2.5.17/dist/vue.js"></script>
        <script>

            window.vm = new Vue({
                data() {
                    return {
                        orders: {!! $orders !!}
                    }

                },
                created() {
                    window.Echo.private(`users-orders.${userId}`)
                        .listen('OrderStatusChanged', (order) => {
                                console.log(order)
                                this.orders = this.orders.map((e) => e.id===order.id ? order : e)
                                                    });

                }
            }).$mount('#app')


        </script>
    @endpush
@endsection
