<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 10 Aug 2018 17:19:07 +0000.
 */

namespace App;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Order
 * 
 * @property int $id
 * @property int $users_id
 * @property int $deliver_users_id
 * @property int $status_orders_id
 * @property \Carbon\Carbon $date_time_order
 * @property \Carbon\Carbon $date_time_order_delivered
 * @property int $total
 * 
 * @property \App\User $user
 * @property \App\StatusOrder $status_order
 * @property \Illuminate\Database\Eloquent\Collection $pizzas
 * @property \Illuminate\Database\Eloquent\Collection $payments
 *
 * @package App
 */
class Order extends Eloquent
{
	public $timestamps = true;

	protected $casts = [
		'users_id' => 'int',
		'deliver_users_id' => 'int',
		'status_orders_id' => 'int',
		'total' => 'int',
        'iva' => 'int'
	];

	protected $dates = [
		'date_time_order',
		'date_time_order_delivered'
	];

	protected $fillable = [
		'users_id',
		'deliver_users_id',
		'status_orders_id',
		'date_time_order_delivered',
		'total',
        'deliver_address_info',
        'deliver_lat',
        'deliver_lng',
        'iva'
	];


    public function orderPizzas() {
	    return $this->hasMany(OrdersPizza::class,'orders_id');
    }
	public function user()
	{
		return $this->belongsTo(\App\User::class, 'users_id');
	}

    public function delivery() {
        return $this->hasOne(\App\Delivery::class,'orders_id');
    }
	public function getTotalorderAttribute() {
        return round((($this->iva/100)*$this->total)+$this->total,2);
    }
	public function getStatusAttribute() {
        $status = $this->status_order;
        return $status->status;
    }
	public function status_order()
	{
		return $this->belongsTo(\App\StatusOrder::class, 'status_orders_id');
	}
    public function status()
    {
        return $this->belongsTo(\App\StatusOrder::class, 'status_orders_id');
    }
	public function pizzas()
	{
		return $this->belongsToMany(\App\Pizza::class, 'orders_pizza', 'orders_id', 'pizzas_id')
					->withPivot('id');
	}

	public function payments()
	{
		return $this->hasOne(\App\Payment::class, 'orders_id');
	}
}
