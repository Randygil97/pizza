<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 10 Aug 2018 17:19:07 +0000.
 */

namespace App;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class PizzasCategory
 * 
 * @property int $id
 * @property string $name
 * @property string $description
 * 
 * @property \Illuminate\Database\Eloquent\Collection $pizzas
 *
 * @package App
 */
class PizzasCategory extends Eloquent
{
	public $timestamps = false;

	protected $fillable = [
		'name',
		'description'
	];

	public function pizzas()
	{
		return $this->hasMany(\App\Pizza::class, 'pizzas_categories_id');
	}
}
