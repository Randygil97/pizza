<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePizzaPricesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pizza_prices', function(Blueprint $table)
		{
			$table->timestamps();
			$table->increments('id');
			$table->integer('pizza_id');
			$table->integer('price');
			$table->string('size', 20);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pizza_prices');
	}

}
