<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Pizzas extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'image' => $this->image,
            'prices' => PizzaPrices::collection($this->whenLoaded('pizza_prices')),
            'url_image' => asset("/frontend/images/pizzas/$this->image")


//           'price' => $this->price,
//           'name' => $this->name,
//           'name' => $this->name,
        ];
        //return parent::toArray($request);
    }
}
