<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    protected $toTruncate = ['users'];
    public function run()
    {

//
//        foreach($this->toTruncate as $table) {
//            \DB::table($table)->truncate();
//        }
        $this->call(OptionsTableSeeder::class);

        $this->call(UsersTableSeeder::class);
        $this->call(PizzaAndPizzaPricesSeeds::class);
        $this->call(StatusOrderTableSeeder::class);
        $this->call(PaymentMethodsTableSeeder::class);
        $this->call(RolesPermissionTableSeeder::class);
        $this->call(DeliveriesStatusTableSeeder::class);
        $this->call(DeliverymenTableSeeder::class);




    }

}
