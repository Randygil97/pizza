<?php

namespace App\Http\Controllers\api;

use App\Delivery;
use App\Deliveryman;
use App\Events\DeliveryCreated;
use App\Events\OrderStatusChanged;
use App\Http\Resources\Orders as OrderResource;
use App\Http\Resources\Deliveryman as DeliveryResource;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Order;
use Auth;
use Illuminate\Support\Facades\Event;
class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $order = Order::with([
            'status_order',
            'orderPizzas',
            'orderPizzas.orders_pizza_toppings',
            'orderPizzas.orders_pizza_toppings.pizzas_topping',
            'delivery',
            'delivery.status_delivery',
            'delivery.deliveryman',
            'delivery.deliveryman.user',
            'delivery.deliveryman.user.person',

            'payments',
            'payments.payment_method',
            'user',
            'user.person'

        ]);

        switch($request->filter) {
            case ('todas'):
                //$order = $order->whereBetween('status_orders_id',1,5);
            break;
            case ('activas'):
                $order = $order->whereBetween('status_orders_id',[1,5]);
            break;
            case ('ruta'):
                $order = $order->where('status_orders_id',5);
            break;
            case ('entregadas'):
                $order = $order->where('status_orders_id',6);
            break;
        }
        if (\Auth::user()->hasRole('repartidor')) {

            $deliverid = Auth::user()->deliveryman->id;

            $order = $order->whereHas('delivery',function($query) use ($deliverid){
                $query->where('deliverymen_id',$deliverid);
            });
        }






//        $order = Order::with(['status_order','orderPizzas','user','user.person'])->paginate(7);
        return OrderResource::collection($order->paginate(7));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = Order::with([
            'status_order',
            'orderPizzas',
            'orderPizzas.orders_pizza_toppings',
            'orderPizzas.orders_pizza_toppings.pizzas_topping',
            'delivery',
            'delivery.status_delivery',
            'payments',
            'payments.payment_method',
            'user',
            'user.person'

        ])->findOrFail($id);


        return new OrderResource($order);


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Order $order
     * @return array
     */
    public function update(Request $request, Order $order) {
//        return $request->all();
        $order->fill($request->all());
        $order->update();
        new event(OrderStatusChanged::broadcast($order->id,$order->user));

        $response = ['success' => true];
        return $response;

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    /**
     * Devuelve los repartidores
     */
    public function getRepartidores() {
//        $repartidores = User::role('repartidor')->with(['deliverymen','deliverymen'])->whereHas('deliverymen',function($query) {$query->where('status',1);})->get();

        $repartidores = Deliveryman::with('user')->where('status',1)->get();

        return DeliveryResource::collection($repartidores);
    }

    public function asignarRepartidor(Request $request) {


        $order = Order::findOrFail($request->order_id);
        $deliveryman = Deliveryman::findOrFail($request->id);
        $deliveries = $deliveryman->whereHas('deliveries',function($query) { $query->where('status','!=',3); })->count();

        if ($deliveries >= 1) {

            return response()->json([
            'success' => false,
            'error' => 'Ya el repartidor superó el limite de entregas pendientes'
            ]);
        }
        $user_id = $deliveryman->user->id;
        $delivery = Delivery::create([
           'orders_id' => $request->order_id,
           'deliverymen_id' => $request->id,
           'status_deliveries_id' => 1
        ]);
        $order->status_orders_id = 5;
        $order->update();

        new event(OrderStatusChanged::broadcast($order->id,$order->user));

        new event(DeliveryCreated::broadcast($order->id,$user_id));
        return ['success' => true];




    }
    public function updateDeliveryStatus(Request $request,$id) {
        $order = Order::findOrFail($id);

        $delivery = $order->delivery;
        if ($request->status_deliveries_id==3) {
            
            $delivery->delivered_time = \Carbon\Carbon::now();
        }
        $delivery->status_deliveries_id = $request->status_deliveries_id;

        $delivery->update();
        new event(OrderStatusChanged::broadcast($order->id,$order->user));

        $status = $delivery->status_delivery;
        return ['success' => true,'status' => $status];

    }
}
