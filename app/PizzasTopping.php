<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 10 Aug 2018 17:19:07 +0000.
 */

namespace App;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class PizzasTopping
 * 
 * @property int $id
 * @property string $name
 * @property int $price
 * 
 * @property \Illuminate\Database\Eloquent\Collection $orders_pizza_toppings
 *
 * @package App
 */
class PizzasTopping extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'price' => 'int'
	];

	protected $fillable = [
		'name',
        'description',
		'price'
	];

	public function orders_pizza_toppings()
	{
		return $this->hasMany(\App\OrdersPizzaTopping::class, 'pizza_toppings_id');
	}
}
